#!/bin/env python
#----------------------------------------------------------------------------
# Name:         TerminalDlg.py
# Author:       XXXX
# Created:      XX/XX/XX
# Copyright:    
#----------------------------------------------------------------------------

from control_wdr import *
import wx
from comunicacion import *
import ErrorDlg
import validators
from configobj import ConfigObj

# WDR: classes

class TerminalDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)

        TerminalDialogFunc( self, True )
        self.CentreOnParent()
        
        self.MeasurementConfig = ConfigObj()
        self.MeasurementConfig.merge(parent.DefaultConfig)
        self.MeasurementConfig.merge(parent.CurrentConfig)




    # WDR: handler declarations for TerminalDlg
        self.Bind(wx.EVT_BUTTON, self.OnEnviar, id=ID_TERMINAL_SEND_BUTTON)
    # WDR: handler implementations for TerminalDlg

    def OnEnviar(self, event):
        Orden = instruccion()
        Orden.Handle = int(self.GetHandleTextctrl().GetValue())
        Orden.Codigo = int(self.GetCodeTextctrl().GetValue())
        Orden.Param[0] =  float(self.GetParameter0Textctrl().GetValue())
        Orden.Param[1] =  float(self.GetParameter1Textctrl().GetValue())
        Orden.Comando = self.GetCommandTextctrl().GetValue().encode('ascii','replace')
        Orden.Instruc = self.GetInstructionTextctrl().GetValue().encode('ascii','replace')

        Respuesta = instruccion()
        try:
            # comunication
            self.IPAddress = self.MeasurementConfig["comunication"]["ip"]
            self.IPPort = int(self.MeasurementConfig["comunication"]["port"])
            self.Sock = diffracsocket(self.IPAddress,self.IPPort)
            self.Sock.Client(Orden,Respuesta)
        except ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)
            return None
        except InstruccionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)
            return None

        self.GetHandleTextctrl().SetValue(str(Respuesta.Handle))
        self.GetCodeTextctrl().SetValue(str(Respuesta.Codigo))
        self.GetParameter0Textctrl().SetValue(str(Respuesta.Param[0]))
        self.GetParameter1Textctrl().SetValue(str(Respuesta.Param[1]))
        self.GetCommandTextctrl().SetValue(Respuesta.Comando)
        self.GetInstructionTextctrl().SetValue(Respuesta.Instruc)
        if Respuesta.Codigo == CODEERROR:
            tmp = "ERROR. "
        else:
            tmp = "O.K. "
        tmp += Respuesta.Instruc
        self.GetAnswerTextctrl().AppendText(tmp)

    #----------------------------------------
    # GETTERS
    #----------------------------------------

    def GetAnswerTextctrl(self):
        return self.FindWindowById(ID_TERMINAL_ANSWER_TEXTCTRL)

    def GetInstructionTextctrl(self):
        return self.FindWindowById(ID_TERMINAL_INSTRUCTION_TEXTCTRL)

    def GetCommandTextctrl(self):
        return self.FindWindowById(ID_TERMINAL_COMMAND_TEXTCTRL)

    def GetParameter1Textctrl(self):
        return self.FindWindowById(ID_TERMINAL_PARAMETER1_TEXTCTRL)

    def GetParameter0Textctrl(self):
        return self.FindWindowById(ID_TERMINAL_PARAMETER0_TEXTCTRL)

    def GetCodeTextctrl(self):
        return self.FindWindowById(ID_TERMINAL_CODE_TEXTCTRL)

    def GetHandleTextctrl(self):
            return self.FindWindowById(ID_TERMINAL_HANDLE_TEXTCTRL)


