pycontrol
---------

A GUI to control a X-ray poweder diffractometer.

This program was developed during my PhD. Thesis. It is build in client/server 
architecture; where the low control of the diffractometer is carried out by a 
embeded CPU (`control' program in C++), and TCP protocol is used to communicate with the high level control
done by pycontrol application.

pycontrol is writen in python using (3700 loc) using the wxPython GUI library.