import sys
import os.path

from configobj import ConfigObj


#------------------------------------------------------------
def loadimage(filename):
    fname = os.path.join(BASEPATH,"images",filename)
##     if not os.path.isfile(fname):
##         return None
    return fname

def loadbasefile(filename):
    fname = os.path.join(BASEPATH,filename)
##     if not os.path.isfile(fname):
##         return None
    return fname

def loadfileat(dir,filename):
    fname = os.path.join(BASEPATH,dir,filename)
##     if not os.path.isfile(fname):
##         return None
    return fname

#------------------------------------------------------------


global BASEPATH
BASEPATH = sys.path[0]

global USERCONF
USERCONF = ConfigObj(loadbasefile("site.cfg"))

## global CURRENTUSER,CURRENTDIR
## CURRENTUSER = USERCONF["user-info"]["currentusername"]
## CURRENTDIR = USERCONF["user-info"]["currentdir"]

DATAFILEEXTENSION = ".xyz"
