from control_wdr import *
import wx

import validators
import comunicacion
import ErrorDlg
import random

from configobj import ConfigObj


ID_TIMER = wx.NewId()

class DriveCirclesDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        DriveCirclesDlgFunc( self, True )

        self.FindWindowById(wx.ID_CLOSE).SetDefault()
        
        # timer
        self.timer = wx.Timer(self,ID_TIMER)
        self.timer.Start(2000)
        
        # config data

        self.CurrentConfig = parent.CurrentConfig
        DefaultConfig = parent.DefaultConfig
        self.MeasurementConfig = ConfigObj()
        self.MeasurementConfig.merge(DefaultConfig)
        self.MeasurementConfig.merge(self.CurrentConfig)



        self.IPAddress = self.MeasurementConfig["comunication"]["ip"]
        self.IPPort = int(self.MeasurementConfig["comunication"]["port"])

        
        try:
            self.Diffractometer = comunicacion.diffracsocket(self.IPAddress,
                                                             self.IPPort)
        except:
            ErrorDlg.ErrorMessage(self,"Network error.")
            self.Diffractometer = None
#            self.OnClose(None)
            
        #self.RefreshCurrent()

        if self.Diffractometer:
            self.InitializeTables()
        else:
            buttonlist = [ID_DRIVECIRCLES_SYNCRONIZE_BUTTON,
                          ID_DRIVECIRCLES_VIEWING_BUTTON,
                          ID_DRIVECIRCLES_MOVETO_BUTTON,
                          ID_DRIVECIRCLES_DEFINEAS_BUTTON,
                          ID_TH_ZERO_BUTTON,
                          ID_TTH_ZERO_BUTTON]
            for button in buttonlist:
                win = self.FindWindowById(button)
                win.Disable()
                pass

##         textctrl = self.GetRequieredTextctrl()
##         textctrl.SetValue("0, 0")
##         textctrl.SetInsertionPoint(2)

        #----------------------------------------
        # EVENT TABLE
        #________________________________________
        
        self.Bind(wx.EVT_TIMER,self.RefreshCurrent, id=ID_TIMER)
        self.Bind(wx.EVT_BUTTON, self.OnSyncronize, id=ID_DRIVECIRCLES_SYNCRONIZE_BUTTON)
        self.Bind(wx.EVT_BUTTON, self.OnViewing, id=ID_DRIVECIRCLES_VIEWING_BUTTON)
        self.Bind(wx.EVT_BUTTON, self.OnMoveTo, id=ID_DRIVECIRCLES_MOVETO_BUTTON)
        self.Bind(wx.EVT_BUTTON, self.OnDefineas, id=ID_DRIVECIRCLES_DEFINEAS_BUTTON)
        self.Bind(wx.EVT_BUTTON, self.OnClose, id=wx.ID_CLOSE)

        self.Bind(wx.EVT_BUTTON, self.OnThZero, id=ID_TH_ZERO_BUTTON)
        self.Bind(wx.EVT_BUTTON, self.OnTThZero, id=ID_TTH_ZERO_BUTTON)


    #----------------------------------------
    # GETTERS
    #----------------------------------------

    def GetRequieredTextctrl(self):
        return self.FindWindowById(ID_DRIVECIRCLES_REQUIERED_TEXTCTRL)

    def GetCurrentTextctrl(self):
        return self.FindWindowById(ID_DRIVECIRCLES_CURRENT_TEXTCTRL)

    

    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnThZero(self, event):
        self.Diffractometer.Zero("th")

    def OnTThZero(self, event):
        self.Diffractometer.Zero("tth")


    def RefreshCurrent(self, event):
        if __debug__:
            tthposition = random.randint(0,180)
            thposition = random.randint(0,180)
        else:
            try:
                tthposition = self.Diffractometer.GetPosition("tth")
                thposition = self.Diffractometer.GetPosition("th")
            except:
                tthposition = None
                thposition = None
        positionstring = str(tthposition)+", "+str(thposition)
        self.GetCurrentTextctrl().SetValue( positionstring)
       

    def OnSyncronize(self, event):
        pass

    def OnViewing(self, event):
        self.Diffractometer.MoveTo("tth",60)


    def InitializeTables(self):
        tthspeed = int(self.MeasurementConfig["tables"]["tth-speed"])
        thspeed = int(self.MeasurementConfig["tables"]["th-speed"])
        self.Diffractometer.TableVelocity("tth",tthspeed)
        self.Diffractometer.TableVelocity("th",thspeed)


    def OnMoveTo(self, event):
        positions = self.GetRequieredTextctrl().GetValue()
        positions = positions.split(",")
        tthpos = positions[0]
        thpos = positions[1]
        try:
            self.Diffractometer.MoveTo("tth",tthpos)
            self.Diffractometer.MoveTo("th",thpos)
        except comunicacion.ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)
        self.RefreshCurrent(event)
        
    def OnDefineas(self, event):
        positions = self.GetRequieredTextctrl().GetValue()
        positions = positions.split(",")
        tthpos = positions[0]
        thpos = positions[1]
        try:
            self.Diffractometer.TableDefineAs("tth",tthpos)
            self.Diffractometer.TableDefineAs("th",thpos)
        except comunicacion.ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)
        self.RefreshCurrent(event)
        




    def OnClose(self,event):
        self.timer.Stop()
        if self.IsModal():
            self.EndModal(wx.ID_OK)
        else:
            self.SetReturnCode(wx.ID_OK);
            self.Show(False);               
        
