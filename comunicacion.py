from timeoutsocket import socket, AF_INET, SOCK_STREAM, setDefaultSocketTimeout, Timeout
#import socket
import struct
import string
import exceptions
from constant import *
#import utilfunc
from time import time, ctime
import logging
import logging.handlers


setDefaultSocketTimeout(20)

# Logging information
comlogger = logging.getLogger('comlog')
loghdlr = logging.handlers.RotatingFileHandler('comunication.log',
                                               maxBytes=5242880,
                                               backupCount=5)
formatter = logging.Formatter('%(asctime)s %(message)s')
loghdlr.setFormatter(formatter)
comlogger.addHandler(loghdlr) 
comlogger.setLevel(logging.DEBUG)

timeoutlogger = logging.getLogger('timeoutlog')
loghdlr = logging.handlers.RotatingFileHandler('timeout.log',
                                               maxBytes=5242880,
                                               backupCount=5)
formatter = logging.Formatter('%(asctime)s %(message)s')
loghdlr.setFormatter(formatter)
timeoutlogger.addHandler(loghdlr) 
timeoutlogger.setLevel(logging.INFO)


class instruccion:

    """Class with the information to send to the diffractometer."""
    
    def __init__(self,Handle=0,Codigo=0,Param=[], Comando="", Instruc=""):

        """Initialize the attributes to null.

        Arguments:
        
        Handle -- A unsigned int (in C language) intended to control
                  the lose of sended instructions.  When sending a
                  bunch of instrucction the sould have diferent (maybe
                  correlative) Handle. Handle = 0 is only for cases
                  when an unique instruction is send, so when Handle is
                  calculed automatically no 0 value can be asigned.
        Codigo -- A int (in C language) that defined the different actions
                  that the diffractometer can do.
        Param --  A list of double (in C language) with 10 elements as maximun.
                  If less tham 10 elements are given, needed element with
                  0 value will be added. This values are used the control
                  the actions of the diffractometer and their mining depend
                  on Codigo.
        Comando -- String (up to 260 characters) eused to define the command
                   when Codigo is 6.
        Instruc -- String (up to 260 characters) used by the diffractometer
                   control software to store the returning value for some
                   commands.
                   
        """

        self.Handle = Handle
        self.Codigo = Codigo
        self.Param = Param
        for i in range(10-len(Param)):
            self.Param.append(0)
        self.Comando = Comando
        self.Instruc = Instruc

    def __str__(self):

        """String representation of instruccion."""
        
        buffer = "Handle = "+str(self.Handle)+"\n"
        buffer += "Codigo = "+str(self.Codigo)+"\n"
        for i in range(len(self.Param)):
            buffer +="Param["+str(i)+"] = "+str(self.Param[i])+"\n"
        buffer += "Comando = "+string.rstrip(self.Comando,"\0")+"\n"
        buffer += "Instruc = "+string.rstrip(self.Instruc,"\0")+"\n"
        return buffer

    def string2struct(self,data):
        
        """
        Converts the string data in a instruccion.

        The conversion is done using the unpack mehtod of the struct
        module. For more information see struct2string method.
        """

        tupla = struct.unpack('Ii10d260s260s',data)
        self.Handle = tupla[0]
        self.Codigo = tupla[1]
        self.Param = list(tupla)[2:12]
        self.Comando = string.rstrip(tupla[12],"\0")
        self.Instruc = string.rstrip(tupla[13],"\0")

    def struct2string(self):

        """
        Convert instruccion in a string to send by UDP socket.

        The data stored in a instruccion is converte in a string using
        the pack method of struct module. The type of each varible is:

        * Handle -- Unsigned int
        * Param  -- Double (There are 10 values)
        * Comando -- String of 260 characters.
        * Instruc -- String of 260 characters.
        """
        
        Handle = self.Handle
        Codigo = self.Codigo
        Param = []
        lenparam = len(self.Param)
        #TODO: create an exception
        if lenparam > 10:
            print "Demasiados parametros"
            return 0
        for i in range(lenparam):
            Param.append(self.Param[i])
        if lenparam < 10:
            for i in range(10-lenparam):
                Param.append(0)
        Comando = self.Comando
        Instruc = self.Instruc
        string = struct.pack('Ii10d260s260s',Handle,Codigo,Param[0],Param[1],
                             Param[2],Param[3],Param[4],Param[5],Param[6],
                             Param[7],Param[8],Param[9],Comando,Instruc)
        return string

class ComunicationError(exceptions.Exception):
    """ Base Exception class for comunication.
    """
    def __init__(self, msg=None):
##        exceptions.Exception.__init__()
        self.Message = msg
    
class ConectionError(ComunicationError):
    """Exception raised for errors in the conection.

    Attributes:
        Message -- explanation of the error
    """
    pass

class InstruccionError(exceptions.Exception):
    """Exception raised for errors in the execution of instruccion.

    """
    def __init__(self, msg=None):
##        exceptions.Exception.__init__()
        self.Message = msg



class TableError(InstruccionError):
    """Exception raised for
    errors in the execution of instruccion related to tables.

    """
    pass

class CommandError(InstruccionError):
    """Exception raised for
    errors in the execution of instruccion related to tables
    movement.

    """
    pass

class GeneratorError(InstruccionError):
    """Exception raised for
    errors in the execution of instruccion related to the generator.

    """
    pass

class ShutterError(InstruccionError):
    """Exception raised for
    errors in the execution of instruccion related to the shutter.

    """
    pass

class DetectorError(InstruccionError):
    """Exception raised for
    errors in the execution of instruccion related to the detector.

    """
    pass

class SampleError(InstruccionError):
    """Exception raised for
    errors in the execution of instruccion related to the sample.

    """
    pass


class diffracsocket:

    """
    A socket class to comunicate with the diffractometer.


    """
    # In diferent machines, different targets to proof
    # TODO: remove it when the program is finished (when??)
    # HOST = "158.227.49.160"
    # PORT = 1020
    #else:
    #    HOST ='localhost'
    #    PORT = 4950
    #addressto = (HOST,PORT)
    MAXRETRANSMIT = 10 # Maximo numero de retrasmisiones de un socket
    MAXHANDLE = 10000 # Handle could be larger but, is not important
    
    def __init__(self,host,port,sock=None):

        """Create a UDP socket for seding to the difractometer."""

        self.addressto = (host,port)
        if sock == None:
##            self.sock = timeoutsocket.TimeSocket(socket.AF_INET, socket.SOCK_DGRAM)
            try:
                self.sock = socket(AF_INET, SOCK_STREAM)
                self.sock.setblocking(1)
            except:
                raise ComunicationError
        else:
            self.sock = sock


        self.sock.connect(self.addressto)
            
        # Handle must be >0 to handle it automatically. Request send by
        # Terminal and with Handle = 0 are not checked
        self.Handle = 1

        # File for log
        #self.LogFile = open("comunication.log","w")

        # How many times a certain client() function is executed after
        # and error is caught.
        self.__clienttry = 0
        

    def Close(self):

        """Close the socket."""

        self.sock.close()
        #self.LogFile.close()
        
    def Read(self,Answer):
        
        """
        Read the response socke.

        Reads the socket sended by the difractometer. The string is unpacket
        into a instruccion type object.
        
        """

        msg = ''

        MSGLEN = 608
        while len(msg) < MSGLEN:
            chunk = self.sock.recv(MSGLEN-len(msg))
            if chunk == '':
                raise RuntimeError,"socket connection broken"
            msg = msg + chunk
        data = msg
        Answer.string2struct(data)

        
    def Send(self,Instruccion):

        """
        Send an truccion to the difracctometer.

        The instruccion type object Instruccion is packet into a
        string and sended to the difractometer by the socket.
        
        """

        self.sock.sendall(Instruccion.struct2string())


    def Client(self,Request,Answer):
        
        try:
            self.SendRead(Request,Answer)
            self.__clienttry = 0
        except InstruccionError:
            self.__clienttry = + 1
            msg = "*** Start Error in comunication ***\n"
            msg += "Try %i time(s)\n" % (self.__clienttry)
            msg += "*** End Error in comunication ***\n"
            comlogger.debug(msg)
#            if self.__clienttry == 5:
            raise 
##             else:
##                 self.Client(Request,Answer)



    def WrapSend(self,Request,times):
        try:
            if times <= 5:
                self.Send(Request)
            else:
                raise ComunicationError
            
        except Timeout:
            times =+ 1
            self.WrapSend(Request,times)


    def WrapRead(self,Answer,times):
        try:
            if times <= 5:
                self.Read(Answer)
            else:
                raise ComunicationError
            
        except Timeout:
            times = times+1
            self.WrapRead(Answer,times)

    def SendRead(self,Request,Answer):

        __sendtimes = 1
        self.WrapSend(Request,__sendtimes)
        if Request.Codigo != 237:
            self.WriteLog("Request",Request)
##         data = self.sock.recv(608)
##         Answer.string2struct(data)


        __readtimes = 1
        self.WrapRead(Answer,__readtimes)
        if int(Answer.Param[9]) != 237:
            self.WriteLog("Answer",Answer)

        RequestCode = Request.Codigo
        AnswerCode = Answer.Codigo
        if AnswerCode == 0:
            if RequestCode in (1,2,3,4,5,32,33,34,35,36,37,40,41):
                raise TableError, Answer.Instruc
            elif RequestCode in (6,):
                raise CommandError, Answer.Instruc
            elif RequestCode in (7,8,9):
                raise GeneratorError, Answer.Instruc
            elif RequestCode in (10,11,12,13):
                raise ShutterError, Answer.Instruc
            elif RequestCode in (14,15,16,17,18,19,20,21,22,23,24,
                                25,26,38):
                raise DetectorError, Answer.Instruc
            elif RequestCode in (39,):
                # The measurement is not finished, so this is not an error.
                pass
            elif RequestCode in (29,30,31):
                raise SampleError, Answer.Instruc
            else:
                raise InstruccionError, Answer.Instruc



    def WriteLog(self,datatype,data):
        msg = "******* %s *************\n" %(datatype)
        msg += str(data)
        comlogger.debug(msg)


    def GetNewHandle(self):
        """Calculate a new handle consecutive to the previous."""
        assert isinstance(self.Handle,int) and self.Handle > 0, "Handle \
        should be a positive integer!"
        NewHandle = self.Handle
        NewHandle += 1
        if NewHandle > self.MAXHANDLE:
            NewHandle = 1
        self.Handle = NewHandle
        return NewHandle

    # Functions to wrap the instructions send to the difractometer.

    # Codigo = 1 CERO_TTH
    # Codigo = 2 CERO_TH
    # Codiog = 3 CERO
    def Zero(self, Mode, Handle = None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        if isinstance(Mode, basestring):
            Mode = Mode.lower()
            if Mode == "tth":
            	Mode = 1
	    elif Mode == "th":
                Request.Codigo = 2
		Mode = 2
            elif Mode == "both":
                Request.Codigo = 3
		Mode = 3
            else:
                raise ValueError, "Error in value Mode."
        if isinstance(Mode, int):
            Mode = Mode
        assert Mode in (1,2,3), "Error in value Mode."
	Request.Codigo = Mode
        self.Client(Request,Answer)
        return Answer

    # Codigo = 4 GG_MM_SS
    # Codigo = 5 GRADOS
    def Visualizacion(self, Mode, Handle=None):
        Mode = Mode.lower()
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        if isinstance(Mode, basestring):
            if Mode == "grados":
                Request.Codigo = 5
            elif Mode == "gg_mm_ss":
                Request.Codigo = 4
            else:
                raise ValueError, "Error in Request.Codigo value."
        if not isinstance(Mode, int):
            Mode = int(Mode)
        assert Mode in (5,4), "Error in Mode Value"
        self.Client(Request,Answer)
        return Answer
        
    # Codigo = 6 mvr command
    def Move(self, Table, Angle, Handle=None):
        Table = Table.lower()
        assert Table in ["th","tth"], "Error in Table value."
        if not isinstance(Angle, float):
            Angle = float(Angle)
        assert ANGLEMIN<=Angle<=ANGLEMAX, "Error in Angle value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 6
        Command = "mvr "
        if Table == "tth":
            Command += "tth "
        elif Table ==  "th":
            Command += "th "
        Command += str(Angle)
        Request.Comando = Command
        self.Client(Request,Answer)
        return Answer

    # Codigo = 6 mv command
    def MoveTo(self, Table, Angle, Handle=None):
        if __debug__:
            print "function MoveTo, parameters:"
            print "Table ",Table
            print "Angle ",Angle
            print "Constrains:"
            print "ANGLEMIN ",ANGLEMIN
            print "ANGLEMAX ",ANGLEMAX
        Table = Table.lower()
        assert Table in ["th","tth"], "Error in Table value."
        if not isinstance(Angle, float):
            Angle = float(Angle)
        assert ANGLEMIN<=Angle<=ANGLEMAX, "Error in Angle value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 6
        Command = "mv "
        if Table == "tth":
            Command += "tth "
        elif Table in "th":
            Command += "th "
        Command += str(Angle)
        Request.Comando = Command
        self.Client(Request,Answer)
        return Answer


    # Coidgo = 7 ENABLE_HV
    def EnableHV(self, Mode, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 7
        if isinstance(Mode,bool):
            if Mode:
                Request.Param[0] = 1
            else:
                Request.Param[0] = 0
        if not isinstance(Mode ,int):
            Mode = int(Mode)
        assert Mode in (0,1), "Error in Mode value."
        self.Client(Request,Answer)
        return Answer

    #----------------------------------------
    # GENERADOR
    #----------------------------------------

    # Codigo = 8 GEN_PROG_VOLTAJE
    # Codigo = 9 GEN_PROG_CORR
    def GenProg(self, Voltage, Current, Handle=None):
        # This if statements allow using strings for the parameters.
        if isinstance(Voltage,basestring):
            Voltage = float(Voltage)
        if isinstance(Current,basestring):
            Current = float(Current)
        if not isinstance(Voltage,float):
            Voltage = float(Voltage)
        if not isinstance(Current, float):
            Current = float(Current)
        assert GENVOLMIN<=Voltage<=GENVOLMAX, "Error in Voltage Value."
        assert GENCURMIN<=Current<=GENCURMAX, "Error in Current Value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 8
        Request.Param[0] = Voltage
        self.Client(Request,Answer)
        Request.Codigo = 9
        Request.Param[0] = Current
        self.Client(Request,Answer)
        return Answer
        
    # Codigo = 10 GET_GEN_VOLTAJE
    def GetVoltageGen(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 10
        self.Client(Request,Answer)
        return Answer

    # Codigo = 11 GET_GEN_CORR
    def GetCurrentGen(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 11
        self.Client(Request,Answer)
        return Answer
   
    #----------------------------------------
    # SHUTTER
    #----------------------------------------

    # Codigo = 12 CLOSE_SHUTTER
    def CloseShutter(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 12
        self.Client(Request,Answer)
        return Answer

    # Codigo = 13 OPEN_SHUTTER
    def OpenShutter(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 13
        self.Client(Request,Answer)
        return Answer

    #----------------------------------------
    # DETECTOR
    #----------------------------------------

    # Codigo = 14 DET_PROG_VOLTAJE
    def ProgVoltageDet(self, Voltage, Handle=None):
        if isinstance(Voltage,basestring):
            Voltage = float(Voltage)
        assert DETVOLMIN<=Voltage<=DETVOLMAX, "Error in Voltage value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 14
        Request.Param[0] = Voltage
        self.Client(Request,Answer)
        return Answer

    # Codigo = 15 GET_DET_VOLTAJE
    def GetVoltageDet(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 15
        try:
            self.Client(Request,Answer)
            return int(Answer.Instruc.split()[0])
        except DetectorError:
            return -1

    #----------------------------------------
    # DETECTOR - AMPLIFICADOR
    #----------------------------------------


    # Codigo = 16 DET:AMPLIFICADOR POLARIDAD
    def PolarityAmplifierDet(self, Polarity, Handle=None):
        if isinstance(Polarity,basestring):
            Polarity = Polarity.lower()
            if Polarity == "positive":
                Polarity = 1
            elif Polarity == "negative":
                Polarity = 0
            else:
                raise ValueError
        if not isinstance(Polarity, int):
            Polarity = int(Polarity)
        assert Polarity in (0,1), "Error in Polarity value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 16
        Request.Param[0] = Polarity
        self.Client(Request,Answer)
        return Answer

    # Codigo = 17 DET:AMPLIFICADOR TIME CTE
    def TimeCteAmplifierDet(self, Tcte, Handle=None):
        if isinstance(Tcte,basestring):
            Tcte = Tcte.lower()
            if Tcte == "t0":
                Tcte = 0
            elif Tcte == "T1":
                Tcte = 1
            else:
                raise ValueError
        if not isinstance(Tcte, int):
            Tcte = int(Tcte)
        assert Tcte in (0,1), "Error in Tcte value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 17
        Request.Param[0] = Tcte
        self.Client(Request,Answer)
        return Answer
    
    # Codigo = 18 DET:AMPLIFICADOR GANANCIA
    def GainAmplifierDet(self, Gain, Handle=None):
        if isinstance(Gain,basestring):
            Gain = int(Gain)
        assert isinstance(Gain,int), "Gain should be int."
        assert GAINMIN<=Gain<=GAINMAX, "Errror in Gain value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 18
        Request.Param[0] = Gain
        self.Client(Request,Answer)
        return Answer

    # Codigo = 19 DET:AMPLIFICADOR PROGRAMAR
    def ProgAmplifierDet(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 19
        self.Client(Request,Answer)
        return Answer
    
    #----------------------------------------
    # DETECTOR - ANALIZADOR
    #----------------------------------------

    # Codigo = 20 DET:ANALIZADOR MODO
    def ModeAnalyserDet(self, Mode, Handle=None):
        if isinstance(Mode, basestring):
            Mode = Mode.lower()
            if Mode == "discriminator":
                Mode = 0
            elif Mode == "analyser 1":
                Mode = 1
            elif Mode == "analyser 2":
                Mode = 2
            else:
                raise ValueError
        if not isinstance(Mode, int):
            Mode = int(Mode)
        assert Mode in (0,1,2), "Error in Mode value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 20
        Request.Param[0] = Mode
        self.Client(Request,Answer)
        return Answer

    # Codigo = 21 DET:ANALIZADOR LOWER LEVEL
    def LLAnalyserDet(self, LL, Handle=None):
        if isinstance(LL,basestring):
            LL = float(LL)
        assert LEVELMIN<=LL<=LEVELMAX, "Error in LL value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 21
        Request.Param[0] = LL
        self.Client(Request,Answer)
        return Answer

    # Codigo = 22 DET:ANALIZADOR UPER  LEVEL
    def ULAnalyserDet(self, UL, Handle=None):
        if isinstance(UL,basestring):
            UL = float(UL)
        assert LEVELMIN<=UL<=LEVELMAX, "Error in UL value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 22
        Request.Param[0] = UL
        self.Client(Request,Answer)
        return Answer

    # Codigo = 23 DET:ANALIZADOR PROGRAMAR
    def ProgAnalyserDet(self, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 23
        self.Client(Request,Answer)
        return Answer

    # Codigo = 24 DET_PROG_MODO
    def ProgModoDet(self, Mode, Handle=None):
        if isinstance(Mode, basestring):
            Mode = Mode.lower()
            if Mode == "time":
                Mode = 1
            elif Mode == "counts":
                Mode = 0
            else:
                raise ValueError
        if not isinstance(Mode, int):
            Mode = int(Mode)
        assert Mode in (0,1), "Error in Mode value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 24
        Request.Param[0] = Mode
        self.Client(Request,Answer)
        return Answer

    # Codgio = 25 DET_PROG_VARLOR
    def ProgDet(self, Value, Handle=None):
        if isinstance(Value, basestring):
            Value = float(Value)
        # The range of correct values of Value depend on the detection
        # type (time or counts)
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 25
        Request.Param[0] = Value
        self.Client(Request,Answer)
        return Answer


    # Codigo = 26 DET_MIDE
    def StartMeasure(self,Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 26
        self.Client(Request,Answer)
        return float(Answer.Instruc.split()[0])

    # Codigo = 42 DET_STOP_MEAS
    def StopMeasure(self,Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 42
        self.Client(Request,Answer)
        return int(Answer.Instruc)
        
    #----------------------------------------
    # SAMPLEHOLDER
    #----------------------------------------

    # Codigo = 29 MUESTRAACTIVA
    # TODO: Comprovar portamuestras con tipo de muestra
    def ActiveSampleHolder(self, SampleHolder, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 29
        if isinstance(SampleHolder, basestring):
            SampleHolder = SampleHolder.lower()
            if SampleHolder == "flat":
                SampleHolder = 0
            elif SampleHolder == "capilar":
                SampleHolder = 1
            else:
                raise ValueError
        if not isinstance(SampleHolder, int):
            SampleHolder = int(SampleHolder)
        assert SampleHolder in (0,1), "Error in SampleHolder value."
        Request.Param[0] = SampleHolder
        self.Client(Request,Answer)
        return Answer

    # Codigo = 30 GIROMUESTRA
    def SpinSampleHolder(self, Speed, Handle=None):
        if isinstance(Speed, basestring):
            Speed = float(Speed)
        # TODO: What speed is allowed?
        if Handle is None:
             Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 30
        Request.Param[0] = Speed
        self.Client(Request,Answer)
        return Answer
        
    # Codigo = 31  PARAR MUESTRA
    def StopSampleHolder(self, Handle=None):
        if Handle is None:
              Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 31
        self.Client(Request,Answer)
        return Answer

    #----------------------------------------
    # TABLES
    #----------------------------------------
         
    # Codigo = 32 TH_VELOCIDAD
    # Codigo = 33 TTH_VELOCIDAD
    def TableVelocity(self, Table, Speed, Handle=None):
        if isinstance(Speed, basestring):
            Speed = float(Speed)
        assert TABSPEEDMIN<=Speed<=TABSPEEDMAX, "Error in Speed value."
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        if isinstance(Table, basestring):
            Table = Table.lower()
            if Table == "tth":
                Request.Codigo = 33
            elif Table == "th":
                Request.Codigo = 32
            else:
                raise ValueError
        Request.Param[0] = Speed
        self.Client(Request,Answer)
        return Answer

    # Codigo = 34 GET_MESA_POSITION
    def GetPosition(self, Table, Mode=DEGREE, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 34
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else:
            raise ValueError
        # TODO, implementar formato gg:mm:ss:
        Request.Param[1] = Mode
        self.Client(Request,Answer)
        return float(Answer.Instruc)
#        return utilfunc.RoundAngle(float(Answer.Instruc.split()[0]))

    # Codigo = 35 SET_HOME
    def SetHome(self, Table, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 35
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else: 
            raise ValueError 
        self.Client(Request,Answer)
        return Answer

    # Codigo = 36 GET_STAT_HOME
    def GetStatHome(self, Table, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 36
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else:
            raise ValueError
        self.Client(Request,Answer)
        return int(Answer.Instruc.split()[0])

    # Codigo = 37 GET_ESTADO_MESA
    def GetTableMovement(self, Table, Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 37
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else:
            raise ValueError
        self.Client(Request,Answer)
        return int(Answer.Instruc.split()[0])
    
    # Codigo = 38 GET_ESTADO_MEDIDA
    def GetStateMeasurement(self,Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 38
        self.Client(Request,Answer)
        return int(Answer.Instruc.split()[0])
    
    # Codigo = 39 GET_MEDIDA
    def GetMeasurement(self,Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 39
        try:
            self.Client(Request,Answer)
            returnvalue = int(Answer.Instruc)
        except ValueError:
            returnvalue = -1
        return returnvalue

    # Codigo = 40 SET_TABLE_TOLERANCE
    def SetTableTolerance(self, Table, Tolerance,  Handle=None):
        if Handle is None:
            Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 40
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else:
            raise ValueError
        if not isinstance(Tolerance, float):
            raise ValueError, "Tolerance should be float"
        Request.Param[1] = Tolerance
        self.Client(Request,Answer)

    # Codigo = 41 MESA_STOP
    def StopTable(self, Table, Handle=None):
        if Handle is None: 
           Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 41
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else:
            raise ValueError
        self.Client(Request,Answer)
        return int(Answer.Instruc.split()[0])
    
    # Codigo = 42 TABLE_DEFINE_AS
    def TableDefineAs(self, Table, Position, Handle=None):
        if Handle is None: 
           Handle = self.GetNewHandle()
        Request = instruccion(Handle)
        Answer = instruccion()
        Request.Codigo = 43
        if Table in ["TTH", "tth"]:
            Request.Param[0] = TTH 
        elif Table in ["TH", "th"]:
            Request.Param[0] = TH
        else:
            raise ValueError
        Request.Param[1] = float(Position)
        self.Client(Request,Answer)
        return
