from  control_wdr import *
import wx
import time
import comunicacion

##from control import BASEPATH

# Dialogs
import ErrorDlg

import DetectorDlg
import GeneratorDlg
import TitleDlg
import StepScanDlg
import OmegaStepScanDlg
import ScanModeDlg
import EditRangeDlg
import MeasureDlg
import DriveCirclesDlg
import TablesHolderDlg
import TerminalDlg
import GraphicDlg
from Globals import *

import os
import os.path
import shutil
import validators



class DiffractometerFrame(wx.Frame):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = (600,400),
        style = wx.DEFAULT_FRAME_STYLE):
        wx.Frame.__init__(self, parent, id, title, pos, size, style)

        # Pass config values to variables
        # Current dir
        self.CURRENTUSER = USERCONF["user-info"]["currentusername"]
        self.CURRENTDIR = USERCONF["user-info"]["currentdir"]

        # if self.CURRENTDIR is not a valid directory use instead
        # working direcotry
        if not os.path.isdir(self.CURRENTDIR):
            self.CURRENTDIR = os.getcwd()

        self.commondefaultfile = loadbasefile("defaults.cfg")
        self.userdefaultfile = loadfileat(self.CURRENTDIR,"defaults.cfg")
        if not os.path.isfile(self.userdefaultfile):
            shutil.copy(self.commondefaultfile,self.CURRENTDIR)

        self.DefaultConfig = parent.DefaultConfig
        self.CurrentConfig = ConfigObj(self.userdefaultfile)

        self.parent = self.GetParent()
        # Data is pased to instances of DataHolder to use with validator.



        # comunication
        self.IPAddress = validators.DataHolder(self.DefaultConfig["comunication"]["ip"])
        self.IPPort = validators.DataHolder(int(self.DefaultConfig["comunication"]["port"]))
        
        self.SetValidators()

        self.IPAddressvalue = self.IPAddress.data
        self.IPPortvalue = self.IPPort.data
        
        self.OpenedFile = None
        self.Datadir = None

        self.panel = wx.Panel(self,-1)
        self.textbox = wx.TextCtrl(self, -1, "",(-1,-1),
                             self.FindWindowById(self.GetId()).GetSize()
                             ,wx.TE_MULTILINE | wx.TE_READONLY)
        self.PrintResume(self.CurrentConfig)

        self.CenterOnParent()

        # Menu Bar
        self.SetMenuBar(DFMenuBarFunc())

        # Tool Bar
        tb = self.CreateToolBar()
        DFToolBarFunc(tb)


       #----------------------------------------
        # EVENT TABLE FILE MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuOpen, id=ID_FILE_OPEN_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuSave, id=ID_FILE_SAVE_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuSaveas, id=ID_FILE_SAVEAS_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuRestore, id=ID_FILE_RESTORE)
        self.Bind(wx.EVT_MENU, self.OnMenuQuit, id=ID_FILE_QUIT_MENU)

        #----------------------------------------
        # EVENT TABLE SETUP MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuDetector, id=ID_SETUP_DETECTOR_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuGenerator, id=ID_SETUP_GENERATOR_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuTablesHolder, id=ID_SETUP_TABLESHOLDER_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuTitle, id=ID_SETUP_TITLE_MENU)

        #----------------------------------------
        # EVENT TABLE DIFFRACTOMETER MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuOpenShutter, id=ID_DIFFRACTOMETER_OPENSHUTTER_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuCloseShutter, id=ID_DIFFRACTOMETER_CLOSESHUTTER_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuStartSample, id=ID_DIFFRACTOMETER_STARTSAMPLE_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuStopSample, id=ID_DIFFRACTOMETER_STOPSAMPLE_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuDriveCircles, id=ID_DIFFRACTOMETER_DRIVECIRCLES_MENU)

        #----------------------------------------
        # EVENT TABLE RANGES MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuScanMode, id=ID_RANGES_SCANMODE_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuEditRange, id=ID_RANGES_EDITRANGE_MENU)

        #----------------------------------------
        # EVENT TABLE MEASURE MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuDataCollection, id=ID_MEASURE_DATACOLLECTION_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuStepScan, id=ID_MEASURE_STEPSCAN_MENU)
        self.Bind(wx.EVT_MENU, self.OnMenuOmegaStepScan, id=ID_MEASURE_OMEGASTEPSCAN_MENU)
##         self.Bind(wx.EVT_MENU, self.OnMenuCount, id=ID_MEASURE_COUNT_MENU)

        #----------------------------------------
        # EVENT TABLE TERMINAL MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuTerminal, id=ID_TERMINAL_MENU)

        #----------------------------------------
        # EVENT TABLE PLOT MENU
        #----------------------------------------

        self.Bind(wx.EVT_MENU, self.OnMenuPlot, id=ID_MENU_PLOT_PLOT)


        #----------------------------------------
        # EVENT TOOLBAR
        #----------------------------------------

        self.Bind(wx.EVT_TOOL, self.OnMenuOpenShutter, id=ID_DIFFRACTOMETER_OPENSHUTTHER_TOOL)
        self.Bind(wx.EVT_TOOL, self.OnMenuCloseShutter, id=ID_DIFFRACTOMETER_CLOSESHUTHER_TOOL)
        self.Bind(wx.EVT_TOOL, self.OnMenuStartSample, id=ID_DIFFRACTOMETER_STARTSAMPLE_TOOL)
        self.Bind(wx.EVT_TOOL, self.OnMenuStopSample, id=ID_DIFFRACTOMETER_STOPSAMPLE_TOOL)
        self.Bind(wx.EVT_TOOL, self.OnMenuSave, id=ID_SAVE_TOOL)
        self.Bind(wx.EVT_TOOL, self.OnMenuOpen, id=ID_OPEN_TOOL)

    #----------------------------------------
    # OTHER EVENTS
    #----------------------------------------
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindows)


    def OnCloseWindows(self, event):
        self.Destroy()

   #----------------------------------------
    # EVENT METHOS FILE MENU
    #----------------------------------------
        
    def OnMenuQuit(self, event):
        self.Close(True)

    def OnMenuOpen(self, event):
        dlg = wx.FileDialog(self, "Chose a file",self.CURRENTDIR,"",
                            "*.*",wx.OPEN | wx.CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            self.OpenedFile = dlg.GetFilename()
            self.Datadir = dlg.GetDirectory()
            self.CURRENTDIR = self.Datadir
            filename = os.path.join(self.Datadir,self.OpenedFile)
            self.CurrentConfig = ConfigObj(filename)
            self.CurrentConfig["files"]["conffile"] = self.OpenedFile
            datafile = filename.split(".")[0]+DATAFILEEXTENSION
            self.CurrentConfig.write()
            self.PrintResume(self.CurrentConfig)
            self.SetValidators()
        dlg.Destroy()
        
        
    def OnMenuSave(self, event):
        if (os.path.abspath(self.CurrentConfig.filename)
            == os.path.abspath(self.userdefaultfile)):
            self.OnMenuSaveas(event)
        else:
            self.CurrentConfig.write()

    # Restore the defauls configuration values of the diffractometer to
    # the configuration file of the user directory.
    def OnMenuRestore(self, event):
    #        shutil.copy(self.commondefaultfile,self.CURRENTDIR)
    #        self.CurrentConfig = ConfigObj(self.userdefaultfile)
        self.CurrentConfig.merge(self.DefaultConfig)
        del self.CurrentConfig['comunication']
        self.CurrentConfig.write()
        self.PrintResume(self.CurrentConfig)

    def OnMenuSaveas(self, event):
        dlg = wx.FileDialog(self, "Chose a file",self.CURRENTDIR,"",
                            "*.*",wx.SAVE | wx.OVERWRITE_PROMPT)
        if dlg.ShowModal() == wx.ID_OK:
            OpenedPath = dlg.GetPath()
            basename, filename = os.path.split(OpenedPath)
            self.CURRENTDIR = basename
            self.CurrentConfig["files"]["conffile"] = filename
            datafile = filename.split(".")[0]+DATAFILEEXTENSION
            self.CurrentConfig["files"]["datafile"] = datafile
            self.CurrentConfig.filename = OpenedPath
            self.CurrentConfig.write()
            self.PrintResume(self.CurrentConfig)
            self.SetValidators()
        dlg.Destroy()
        
    #----------------------------------------
    # EVENT METHODS SETUP FILE MENU
    #----------------------------------------

    def OnMenuDetector(self, event):
        dlg = DetectorDlg.DetectorDlg(self, -1, "Detector")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()
        
    def OnMenuGenerator(self, event):
        dlg = GeneratorDlg.GeneratorDlg(self, -1, "Generator")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()

    def OnMenuTablesHolder(self, event):
        dlg = TablesHolderDlg.TablesHolderDlg(self, -1,
                                              "Tables and Sample Holder")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()
        
    def OnMenuTitle(self, event):
        dlg = TitleDlg.TitleDlg(self, -1, "Title")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()

    
    #----------------------------------------
    # EVENT METHODS DIFFRACTOMETER MENU
    #----------------------------------------

    def OnMenuOpenShutter(self, event):
        try:
            diffractometer = comunicacion.diffracsocket(self.IPAddress.data,
                                                        self.IPPort.data)
            diffractometer.OpenShutter()
        except comunicacion.ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)           
        

    def OnMenuCloseShutter(self, event):
        try:
            diffractometer = comunicacion.diffracsocket(self.IPAddress.data,
                                                        self.IPPort.data)
            diffractometer.CloseShutter()
        except comunicacion.ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)           

    def OnMenuStartSample(self, event):
        try:
            diffractometer = comunicacion.diffracsocket(self.IPAddress.data,
                                                        self.IPPort.data)
            diffractometer.SpinSampleHolder(self.HolderVelocity)
        except comunicacion.ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)           

    def OnMenuStopSample(self, event):
        try:
            diffractometer = comunicacion.diffracsocket(self.IPAddress.data,
                                                        self.IPPort.data)
            diffractometer.StopSampleHolder()
        except comunicacion.ConectionError, error:
            ErrorDlg.ErrorMessage(self, error.Message)           

    def OnMenuDriveCircles(self, event):
        global DriveCirclesDlg
        reload(DriveCirclesDlg)
        dlg = DriveCirclesDlg.DriveCirclesDlg(self, -1, "Drive Circles")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()


    #----------------------------------------
    # EVENT METHOS RANGES MENU
    #----------------------------------------

    def OnMenuScanMode(self, event):
        reload(ScanModeDlg)
        dlg = ScanModeDlg.ScanModeDlg(self, -1, "Scan Mode")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()


    def OnMenuEditRange(self, event):
        reload(EditRangeDlg)
        dlg = EditRangeDlg.EditRangeDlg(self, -1, "Edit Range")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()


    #----------------------------------------
    # EVENT METHOS PLOT MENU
    #----------------------------------------

    def OnMenuPlot(self, event):
        global GraphicDlg
        reload(GraphicDlg)
        dlg = GraphicDlg.GraphicDlg(self, -1, "Plot")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()


    #----------------------------------------
    # EVENT METHOS MEASURE MENU
    #----------------------------------------

    def OnMenuDataCollection(self, event):
        global MeasureDlg
        reload(MeasureDlg)
        dlg = MeasureDlg.MeasureDlg(self, -1, "Measurement")
        if dlg.ShowModal() == wx.ID_OK:
            self.PrintResume(self.CurrentConfig)
        dlg.Destroy()


    def OnMenuStepScan(self, event):
        global StepScanDlg
        reload(StepScanDlg)
        dlg = StepScanDlg.StepScanDlg(self, -1, "Step Scan")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()

    def OnMenuOmegaStepScan(self, event):
        global OmegaStepScanDlg
        reload(OmegaStepScanDlg)
        dlg = OmegaStepScanDlg.OmegaStepScanDlg(self, -1, "Omega Step Scan")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()


    def OnMenuCount(self, event):
        pass

    #----------------------------------------
    # EVENT METHOS TERMINAL MENU
    #----------------------------------------

    def OnMenuTerminal(self, event):
        global TerminalDlgDlg
        reload(TerminalDlg)
        global comunicacion
        reload(comunicacion)
        dlg = TerminalDlg.TerminalDlg(self, -1, "Terminal")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()


    #----------------------------------------
    # METHODS
    #----------------------------------------

    def PrintResume(self,ConfigurationObject):
        text='''
Configuration file:\t%s
Data file:\t\t%s
Measurement started at\t\t%s
Measurement finished at\t%s
Measurement duration\t%s
Title:\t\t\t%s
Diffractometer:\t%s - %s\t\tMonocromator:\t%s
Ranges \tTheta:\t%s %s %s
\t\tOmega:\t%s %s %s
%s seg.
Radiation:\t%s \t\t Generator:%skV,%smA
Detector:\t%s
'''%(ConfigurationObject["files"]["conffile"],
     ConfigurationObject["files"]["datafile"],
     ConfigurationObject["times"]["startedat"],
     ConfigurationObject["times"]["finishedat"],
     ConfigurationObject["times"]["duration"],
     self.TitleTitle.data,
     ConfigurationObject["scanmode"]["mode"],
     ConfigurationObject["scanmode"]["type"],
     "None",
     ConfigurationObject["range"]["2thetabegin"],     
     ConfigurationObject["range"]["2thetaend"],     
     ConfigurationObject["range"]["2thetastep"],     
     ConfigurationObject["range"]["thetabegin"],     
     ConfigurationObject["range"]["thetaend"],     
     ConfigurationObject["range"]["thetastep"],
     ConfigurationObject["range"]["timestep"],     
     "Cu",
     ConfigurationObject['generator']['voltage'],
     ConfigurationObject['generator']['current'],
     "Scintillator")

        self.textbox.SetValue(text)


    def SetValidators(self):
        # Generator section
        self.GeneratorVoltage = validators.DataHolder(int(self.CurrentConfig["generator"]["voltage"]))
        self.GeneratorCurrent = validators.DataHolder(int(self.CurrentConfig["generator"]["current"]))
        self.GeneratorShutdown = validators.DataHolder(self.CurrentConfig["generator"]["shutdown"])
        self.GeneratorShutter = validators.DataHolder(self.CurrentConfig["generator"]["opencloseshutter"])
        self.GeneratorNotXray = validators.DataHolder(self.CurrentConfig["generator"]["notxray"])

        # Detector section
        self.DetectorHighVoltage = validators.DataHolder(int(self.CurrentConfig["detector"]["highvoltage"]))
        self.DetectorLowerLevel = validators.DataHolder(self.CurrentConfig["detector"]["lowerlevel"])
        self.DetectorUpperLevel = validators.DataHolder(self.CurrentConfig["detector"]["upperlevel"])
        self.DetectorGain = validators.DataHolder(self.CurrentConfig["detector"]["gain"])
        self.DetectorPolarity = validators.DataHolder(self.CurrentConfig["detector"]["polarity"])
        self.DetectorTimeCte = validators.DataHolder(self.CurrentConfig["detector"]["tcte"])
        self.DetectorMode = validators.DataHolder(self.CurrentConfig["detector"]["mode"])

        # Title section
        self.TitleTitle = validators.DataHolder(self.CurrentConfig["title"]["title"])
        self.TitleComment = validators.DataHolder(self.CurrentConfig["title"]["comment"])

        # Scan Mode section
        self.ScanModeMode = validators.DataHolder(self.CurrentConfig["scanmode"]["mode"])
        self.ScanModeType = validators.DataHolder(self.CurrentConfig["scanmode"]["type"])
        self.ScanModeDetectionType = validators.DataHolder(self.CurrentConfig["scanmode"]["detectiontype"])
        
        # Edit Range section

        self.EditRange2ThetaBegin = validators.DataHolder(self.CurrentConfig["range"]["2thetabegin"])
        self.EditRange2ThetaEnd = validators.DataHolder(self.CurrentConfig["range"]["2thetaend"])
        self.EditRange2ThetaStep = validators.DataHolder(self.CurrentConfig["range"]["2thetastep"])
        self.EditRangeThetaBegin = validators.DataHolder(self.CurrentConfig["range"]["thetabegin"])
        self.EditRangeThetaEnd = validators.DataHolder(self.CurrentConfig["range"]["thetaend"])
        self.EditRangeThetaStep = validators.DataHolder(self.CurrentConfig["range"]["thetastep"])
        self.EditRangeTimeStep = validators.DataHolder(self.CurrentConfig["range"]["timestep"])
        self.EditRangeCountsStep = validators.DataHolder(self.CurrentConfig["range"]["countsstep"])

        # Tables section
        self.TthetaVelocity = validators.DataHolder(int(self.CurrentConfig["tables"]["tth-speed"]))
        self.ThetaVelocity = validators.DataHolder(int(self.CurrentConfig["tables"]["th-speed"]))
        self.GoHome = validators.DataHolder(self.CurrentConfig["tables"]["go-home"])


        # Sample Holder secton
        self.HolderVelocity = validators.DataHolder(self.CurrentConfig["sampleholder"]["holderspeed"])
        self.IsHolder = validators.DataHolder(self.CurrentConfig["sampleholder"]["isholder"])
