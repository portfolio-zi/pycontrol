import wx

def ErrorMessage(Parent, Message):
    dlg = wx.MessageDialog(Parent, Message, "Error Dialog",
                          wx.OK | wx.ICON_ERROR)
    dlg.ShowModal()
    dlg.Destroy()
