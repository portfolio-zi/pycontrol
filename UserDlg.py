import wx
from control_wdr import *
from wx.lib.filebrowsebutton import DirBrowseButton
from Globals import *
from ErrorDlg import ErrorMessage

class NewUserDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        


        self.db = DirBrowseButton(self, ID_NEWUSER_DIR, size=(550,-1))

        NewUserDlgFunc( self, True )
                                  
        # WDR: handler declarations for NewUserDlg
        self.Bind(wx.EVT_BUTTON, self.OnAdd, id=ID_NEWUSER_ADD_BUTTON)

    def GetNewUserCtrl(self):
        return self.FindWindowById(ID_NEWUSER_USER_TEXTCTRL)

    def GetDirListCtrl(self):
        return self.FindWindowById(ID_NEWUSER_DIR)

    def OnAdd(self,event):
        user = self.GetNewUserCtrl().GetValue()
        dir = self.GetDirListCtrl().GetValue()
        users = USERCONF["users"]["usersnames"]
        if user in users:
            ErrorMessage(self,"User allready in user list.")
        else:
            users.append(user)
            USERCONF["users"]["usersnames"] = users
            newsection = "user-%s" %(user)
            USERCONF[newsection] = {}
            USERCONF[newsection]["dir"] = dir
            USERCONF["user-info"]["currentusername"] = user
            USERCONF["user-info"]["currentdir"] = dir
            USERCONF.write()
            self.Close()



class SelectUserDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)

        self.parent = parent
        SelectUserDlgFunc( self, True )
        
        self.PopulateList()

        self.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnSelect,id=ID_SELECTUSER_LISTCTRL)
        self.Bind(wx.EVT_BUTTON,self.OnSelectButton,id=ID_SELECTUSER_SELECT_BUTTON)


    def OnSelect(self, event):
        self.currentitem = event.m_itemIndex
        self.ActiveUser = self.GetUserDirListCtrl().GetItemText(self.currentitem)
        self.ActiveDir = self.GetUserDirListCtrl().GetItem(self.currentitem,1).GetText()

    def OnSelectButton(self,event):
        USERCONF["user-info"]["currentusername"] = self.ActiveUser
        USERCONF["user-info"]["currentdir"] = self.ActiveDir

        CURRENTUSER = USERCONF["user-info"]["currentusername"]
        CURRENTDIR = USERCONF["user-info"]["currentdir"]

##g        print ">>", CURRENTUSER, CURRENTDIR
##         self.parent.SetStatusText("User: %s,dir=%s" %(CURRENTUSER,CURRENTDIR))


        USERCONF.write()
        self.Close()
        
    def GetUserDirListCtrl(self):
        return self.FindWindowById(ID_SELECTUSER_LISTCTRL)

    def PopulateList(self):
        listctrl = self.GetUserDirListCtrl()
        listctrl.InsertColumn(0,"User")
        listctrl.InsertColumn(1,"Directory")
        
        users = USERCONF["users"]["usersnames"]
        users.sort()
        for x in range(len(users)):
            listctrl.InsertStringItem(x,users[x])
            listctrl.SetStringItem(x,1,
                                   USERCONF["user-%s"%(users[x])]["dir"])
            
        listctrl.SetColumnWidth(0,wx.LIST_AUTOSIZE)
        listctrl.SetColumnWidth(1,wx.LIST_AUTOSIZE)
        
