from constant import *

def RoundAngle(Angle):
    global TABLEPRECISION
    return round(Angle/TABLEPRECISION)*TABLEPRECISION


def gauss(x,x0,sigma):
    """ Gaussian function.
    """
    from scipy import pi, exp,sqrt

    exponent = (x-x0)**2/(2*sigma**2)
    
    return (1/sigma*sqrt(2*pi)) * exp(-1*exponent)

def lorentz(x,x0,sigma):
    """ Lorentzian function.
    """
    from scipy import pi

    numerator = sigma / (2*pi)
    denominator = (x-x0)**2 +(sigma/2)**2
    return (numerator/denominator)

def pseudovoight(x,A,x0,sigma,eta):
    """ Pseudo-Voight funciton.
    """
    from scipy import log,sqrt
    
    sigmagauss = sigma/(2*sqrt(2*log(2)))

    return A*(eta*lorentz(x,x0,sigma)+(1-eta)*gauss(x,x0,sigmagauss))
