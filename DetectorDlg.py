from control_wdr import *
import wx

from validators import SetValidatorsToWidgets


class DetectorDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        DetectorDlgFunc( self, True )
        
        
        # Get default values
        self.CurrentConfig = parent.CurrentConfig

        self.values = {}
        self.values['DetectorHighVoltage'] = ['detector','highvoltage','Slider','Generic']
        self.values['DetectorPolarity'] = ['detector','polarity','Combo','Generic']
        self.values['DetectorTimeCte'] = ['detector','tcte','Combo','Generic']
        self.values['DetectorGain'] = ['detector','gain','TextCtrl','IntegerText']
        self.values['DetectorMode'] = ['detector','mode','Combo','Generic']
        self.values['DetectorLowerLevel'] = ['detector','lowerlevel','TextCtrl','FloatText']
        self.values['DetectorUpperLevel'] = ['detector','upperlevel','TextCtrl','FloatText']

        # Put default values in the dialog.
        for WidgetName in self.values.keys():
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)


        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)


    #----------------------------------------    
    # Getters 
    #----------------------------------------    

    def GetDetectorHighVoltageSlider(self):
        return self.FindWindowById(ID_DETECTOR_HV_SLIDER)

    def GetDetectorPolarityCombo(self):
        return self.FindWindowById(ID_DETECTOR_POLARITY_COMBO)

    def GetDetectorTimeCteCombo(self):
        return self.FindWindowById(ID_DETECTOR_TIMECTE_COMBO)

    def GetDetectorModeCombo(self):
        return self.FindWindowById(ID_DETECTOR_MODE_COMBO)

    def GetDetectorGainTextCtrl(self):
        return self.FindWindowById(ID_DETECTOR_GAIN_TEXTCTRL)

    def GetDetectorLowerLevelTextCtrl(self):
        return self.FindWindowById(ID_DETECTOR_LL_TEXTCTRL)

    def GetDetectorUpperLevelTextCtrl(self):
        return self.FindWindowById(ID_DETECTOR_UL_TEXTCTRL)

    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnOK(self,event):
        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                value = str(eval('self.GetParent().'+item[0]+'.data'))
                self.CurrentConfig[section][option] = value
#            self.CurrentConfig.write()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return


