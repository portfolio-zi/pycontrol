from matplotlib.numerix import arange, sin, pi

import matplotlib

# comment out the following to use wx rather than wxagg
matplotlib.use('WXAgg')
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.backends.backend_wx import _load_bitmap

from matplotlib.figure import Figure

#from wxPython.wx import *
import wx
from control_wdr import *

import os.path


class MyNavigationToolbar(NavigationToolbar2Wx):
    """
    Extend the default wx toolbar with your own event handlers
    """
    ID_GRID_TOOGLE = wx.NewId()
    ID_ON_CLOSE = wx.NewId()
    ID_LABEL = wx.NewId()

    def __init__(self, canvas, cankill):
        NavigationToolbar2Wx.__init__(self, canvas)

        self.canvas = canvas
        # for simplicity I'm going to reuse a bitmap from wx, you'll
        # probably want to add your own.
        grid_icon = wx.Bitmap("grid_icon.xpm", wx.BITMAP_TYPE_XPM)
        self.AddCheckTool(self.ID_GRID_TOOGLE,grid_icon,
                          grid_icon,"Grid","Activate/DesactivateGrid")
        self.AddSimpleTool(self.ID_ON_CLOSE, _load_bitmap('stock_close.xpm'),
                           'Closee', 'Activate custom contol')
        self.AddSeparator()
        self.DataText = wx.StaticText(self,self.ID_LABEL,"Angle:\nIntensity:")
        self.AddControl(self.DataText)


        self.Bind(wx.EVT_TOOL, self._on_grid, id=self.ID_GRID_TOOGLE)
        self.Bind(wx.EVT_TOOL, self._on_close, id=self.ID_ON_CLOSE)

    def _on_close(self, evt):
        self.canvas.GetParent().Close()

    def _on_grid(self,event):
        self.canvas.GetParent().axes.grid()
        self.canvas.draw()


    def save(self, evt):
        # Fetch the required filename and file type.
        filetypes = self.canvas._get_imagesave_wildcards()
        # insert your default dir here
        defaultdir = self._parent.datadir
        defaultfile = os.path.basename(self._parent.datafile).split(".")[0]
        dlg =wx.FileDialog(self._parent, "Save to file", defaultdir, defaultfile, filetypes,
                           wx.SAVE|wx.OVERWRITE_PROMPT|wx.CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            
            dirname  = dlg.GetDirectory()
            filename = dlg.GetFilename()
##             DEBUG_MSG('Save file dir:%s name:%s' % (dirname, filename), 3, self)
            self.canvas.print_figure(os.path.join(dirname, filename))


class GraphicDlg(wx.Dialog):
    def __init__(self, parent, ID, title):
        wx.Dialog.__init__(self, parent, ID, title,size=(400,400),)

        self.parent = parent
        self.CurrentConfig = self.parent.CurrentConfig
        self.datafile = self.CurrentConfig["files"]["datafile"]
        self.datadir = self.parent.CURRENTDIR
        try:
            self.datafile = os.path.join(self.datadir,self.datafile)
        except:
            pass
            
        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.axes.set_xlabel(r'$2 \theta$')
        self.axes.set_ylabel("Intensity")
#        self.axes.grid()
        t = []
        s = []
        
        # Parse the data file
        try:
            datafile = open(self.datafile,"r")
            lines = datafile.readlines()
            datafile.close()
        except IOError:
            lines = ["0 0"]
        for line in lines:
            if not line.startswith("#"):
                data = line.split()
                x = float(data[0])
                y = float(data[1])
                t.append(x)
                s.append(y)

        if len(t) == 0 or len(s) == 0:
            t = [0,180]
            s = [1,1]
                
        # The x axis
        #t = arange(0.0,3.0,0.01)
        # The y axis
        #s = sin(2*pi*t)
        
        self.axes.plot(t,s)
        self.canvas = FigureCanvas(self, -1, self.figure)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.sizer.AddSpacer((0,30))
        self.SetSizer(self.sizer)
        self.Fit()
        self.canvas.mpl_connect("motion_notify_event",self.on_move)

        self.add_toolbar()  # comment this out for no toolbar
        # Add mouse motion event
        # bindig_id = self.toolbar.Connect('motion_notify_event', self._on_move )

    def add_toolbar(self):
        self.toolbar = MyNavigationToolbar(self.canvas,True)
        self.toolbar.Realize()
        if wx.Platform == '__WXMAC__':
            # Mac platform (OSX 10.3, MacPython) does not seem to cope with
            # having a toolbar in a sizer. This work-around gets the buttons
            # back, but at the expense of having the toolbar at the top
            self.SetToolBar(self.toolbar)
        else:
            # On Windows platform, default window size is incorrect, so set
            # toolbar width to figure width.
            tw, th = self.toolbar.GetSizeTuple()
            fw, fh = self.canvas.GetSizeTuple()
            # By adding toolbar in sizer, we are able to put it at the bottom
            # of the frame - so appearance is closer to GTK version.
            # As noted above, doesn't work for Mac.
            self.toolbar.SetSize(wx.Size(fw, th))
            self.sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        # update the axes menu on the toolbar
        self.toolbar.update()  

        
    def OnPaint(self, event):
        self.canvas.draw()
        
    def on_move(self,event):
        # get the x and y pixel coords
        x, y = event.x, event.y

        if event.inaxes:
            ax = event.inaxes  # the axes instance
            angle = event.xdata
            intensity = int(event.ydata)
            buffer = "Angle: %.5f\nIntensity: %i" % (angle,intensity)
            self.toolbar.DataText.SetLabel(buffer)
            self.Refresh()
            


