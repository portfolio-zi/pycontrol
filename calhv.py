from comunicacion import *
import time

def SetHV(Diffractometer,hv):
    Diffractometer.ProgVoltageDet(hv)
    while 1:
            DetVoltage = Diffractometer.GetVoltageDet()
#            print "##",DetVoltage, type(DetVoltage)
            # GetVoltage return -1 when the voltage is not set yet.
            if DetVoltage == -1:
                time.sleep(0.5)
            else:
                return

def GetInt(Diffractometer):
    Diffractometer.StartMeasure()
    while 1:
        Intensity = Diffractometer.GetMeasurement()
#        print "#",Intensity,type(Intensity)
        # GetMeasurement returns -1 when measurement not finished.
        if Intensity >= 0:
            return Intensity


Diff = diffracsocket("158.227.49.160",1020)


for hv in range(1000,2000,10):
    SetHV(Diff,hv)
    int = GetInt(Diff)
    print hv, int


