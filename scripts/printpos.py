"""Print position of 2Theta and Theta circles.
"""

# import comunication module
from comunicacion import *

# Open a socket to the diffractometer
DifSock = diffracsocket("158.227.49.160",1020)

tthpos =  DifSock.GetPosition("tth")
thpos = DifSock.GetPosition("th")

print tthpos,thpos
