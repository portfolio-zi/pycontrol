from constant import *
import math
#from wxPython.wx import *
import wx
from Events import *
import ErrorDlg
import time
import threading
import Queue
import sys
import traceback

import logging
import logging.handlers

# Logging information
melogger = logging.getLogger('mainlog.measurelog')
loghdlr = logging.handlers.RotatingFileHandler('measurement.log',
                                               maxBytes=5242880,
                                               backupCount=5)
formatter = logging.Formatter('%(asctime)s %(message)s')
loghdlr.setFormatter(formatter)
melogger.addHandler(loghdlr) 
melogger.setLevel(logging.DEBUG)



class MeasurementBasic(object):
    """Basic mehtods for the measurement.

    This class deal with the following tasks:
    * Set the generator
    * Enable/Disable the generator
    * Set the detector (except the detection type (time or counts) wich is
      for handle by the DetectionType class.
    * Do the measure

    This class need some attributes that controls the behaviour of the
    measurement:

    * ShutterAllwaysOpened
    """
    def __init__(self, MeasurementConfig):
        #self.Diffractometer = Diffractometer
        self.Points = []

        self.GenVoltage = MeasurementConfig["generator"]["voltage"]
        self.GenCurrent = MeasurementConfig["generator"]["current"]
        self.GenShutdown = MeasurementConfig["generator"]["shutdown"]
        self.OpenCloseShutter = eval(MeasurementConfig["generator"]["opencloseshutter"])
        self.DebugXray = eval(MeasurementConfig["generator"]["notxray"])
        self.ThereisSample = eval(MeasurementConfig["sampleholder"]["isholder"])

        self.DetVoltage = MeasurementConfig["detector"]["highvoltage"]
        self.DetAnalyserMode = MeasurementConfig["detector"]["mode"]
        self.DetLL = MeasurementConfig["detector"]["lowerlevel"]
        self.DetUL = MeasurementConfig["detector"]["upperlevel"]
        self.DetPolarity = MeasurementConfig["detector"]["polarity"]
        self.DetTimeCte =  MeasurementConfig["detector"]["tcte"]
        self.DetGain = MeasurementConfig["detector"]["gain"]

        self.ThetaAccuracy = float(MeasurementConfig["tables"]["tth-accuracy"])
        self.TthetaAccuracy = float(MeasurementConfig["tables"]["th-accuracy"])
        
    def SetDetector(self): # Detector Voltage
        self.Diffractometer.ProgVoltageDet(self.DetVoltage)
        # Whait untill the voltage is set 
        while 1:
            DetVoltage = self.Diffractometer.GetVoltageDet()
            # GetVoltage return -1 when the voltage is not set yet.
            if DetVoltage != -1:
                break
            time.sleep(0.5)
        # Detector Amplifier
        self.Diffractometer.PolarityAmplifierDet(self.DetPolarity)
        self.Diffractometer.GainAmplifierDet(self.DetGain)
        self.Diffractometer.ProgAmplifierDet() # Detector Analyser
        self.Diffractometer.ModeAnalyserDet(self.DetAnalyserMode)
        self.Diffractometer.LLAnalyserDet(self.DetLL)
        self.Diffractometer.ULAnalyserDet(self.DetUL)
        self.Diffractometer.ProgAnalyserDet() # Detector Mode -- Time/Pulse
        self.SetDetectorMode()

    def SetGenerator(self):
        self.Diffractometer.GenProg(self.GenVoltage,self.GenCurrent)

    def EnableGenerator(self):
        self.Diffractometer.EnableHV(True)
        # Time needed to enable High Voltage (4 seconds)
        time.sleep(4) 

    def DisableGenerator(self):
        self.Diffractometer.EnableHV(False)

    def SetTableVelocity(self,table,step):
        if step > 1:
            vel = 100
        elif step <= 0.01:
            vel = 33
        elif step <= 1:
            vel = 33
        self.Diffractometer.TableVelocity(table,vel)


    def FinalizeTables(self):
        "Should be implemented by inherited objects."
        pass

    
    def Measure(self):
        self.InitializeTables()
        melogger.info("InitializeTables done.")
        if not self.DebugXray:
            self.SetDetector()
            melogger.info("SetDetector done.")
            self.SetGenerator()
            melogger.info("SetGenerator done.")
        if self.ThereisSample:
            self.StartSampleHolder()
            melogger.info("StartSampleHolder done.")
        if not self.DebugXray:
            self.EnableGenerator()
            melogger.info("EnableGenerator done.")
        self.PositionActual = self.GetPosition()
        melogger.info("Get PositionAcutal done. done.")
        while not self.MeasurementFinished():
            if self._want_abort:
                return
            self.Diffractometer.OpenShutter()
            melogger.info("OpenShutter done")
            self.Diffractometer.StartMeasure()
            melogger.info("StartMeasure done")
            while 1:
                if self._want_abort:
                    return
                Intensity = self.Diffractometer.GetMeasurement()
                melogger.info("GetMeasurement done")
                # GetMeasurement returns -1 when measurement not finished.
                if Intensity >= 0:
                    break
            if  self.OpenCloseShutter:
                self.Diffractometer.CloseShutter()
            Point = (self.PositionActual, Intensity)
            # EVENT
            wx.PostEvent(self._notify_window, GetPointEvent(Point))
            melogger.info("wx.PostEvent done")
            #wxYield()
            #melogger.info("wxYield done")
            if self._want_abort:
                return
            self.Points.append(Point)
            self.MoveToNextPoint()
            self.PositionActual = self.GetPosition()
            melogger.info("ACT POSITION %f" %(self.PositionActual))
            melogger.info("TARGET POSITON %f" %(self.PositionTarget))
            melogger.info("TARGET POSITION %s" %(str(self.PositionTarget)))
##        self.FinalizeMeasurement()

    def FinalizeMeasurement(self):
        melogger.info("FinalizeMeasurement")
        if not self.DebugXray:
            if self.GenShutdown:
                self.DisableGenerator()
                melogger.info("Generator disabled.")
        if self.ThereisSample:
            self.StopSampleHolder()
            melogger.info("Sample Holder stoped.")
        self.Diffractometer.CloseShutter()
        melogger.info("Shutter closed")
        self.FinalizeTables()
        melogger.info("Tables to initial position.")
        self.Diffractometer.Close()

class ScanType(object):
    """Base class for the Scan Type. This class is subclassed for use.
    Gets the values for the angles from the MeasurementConfig.
    """
    def __init__(self, MeasurementConfig):
        self.ThetaBegin = float(MeasurementConfig["range"]["thetabegin"])
        self.ThetaEnd = float(MeasurementConfig["range"]["thetaend"])
        self.ThetaStep = float(MeasurementConfig["range"]["thetastep"])

        self.TThetaBegin = float(MeasurementConfig["range"]["2thetabegin"])
        self.TThetaEnd = float(MeasurementConfig["range"]["2thetaend"])
        self.TThetaStep = float(MeasurementConfig["range"]["2thetastep"])

class OneTable(ScanType):
    """For scans that only use one table.
    Functions use by Measure():
      * WaitUntilMovementFinished
      * InitializeTables
      * FinalizeTables
    """

    def __init__(self, MeasurementConfig):
        ScanType.__init__(self,MeasurementConfig)

        
class TwoTables(ScanType):
    """For scans that only use two table.
    Functions use by Measure():
      * WaitUntilMovementFinished
      * InitializeTables
      * FinalizeTables
    """
    def __init__(self,MeasurementConfig):
        ScanType.__init__(self,MeasurementConfig)

    def WaitUntilMovementFinished(self):
        while (self.Diffractometer.GetTableMovement("tth") or
               self.Diffractometer.GetTableMovement("th")):
##             if self._want_abort:
##                 self.StopTables()
##                 return
            time.sleep(0.5)
            pass

    def InitializeTables(self):
        self.Diffractometer.SetTableTolerance("tth",self.TthetaAccuracy)
        actualpos = self.Diffractometer.GetPosition("tth")
        step = abs(actualpos-self.TThetaBegin)
        self.SetTableVelocity("tth",step)
        self.PositionTarget = self.TThetaBegin
        self.Diffractometer.MoveTo("tth",self.TThetaBegin)

        self.Diffractometer.SetTableTolerance("th",self.ThetaAccuracy)
        actualpos = self.Diffractometer.GetPosition("th")
        step = abs(actualpos-self.ThetaBegin)
        self.SetTableVelocity("th",step)
        self.Diffractometer.MoveTo("th",self.ThetaBegin)

        self.WaitUntilMovementFinished()

    def StopTables(self):
        self.Diffractometer.StopTable("th")
        self.Diffractometer.StopTable("tth")

    def FinalizeTables(self):
        actualpos = self.Diffractometer.GetPosition("tth")
        step = abs(actualpos-self.TThetaBegin)
        self.SetTableVelocity("tth",step)
        self.Diffractometer.MoveTo("tth",self.TThetaBegin)

        actualpos = self.Diffractometer.GetPosition("th")
        step = abs(actualpos-self.ThetaBegin)
        self.SetTableVelocity("th",step)
        self.Diffractometer.MoveTo("th",self.ThetaBegin)

        self.WaitUntilMovementFinished()


    

class TThetaTheta(TwoTables):
    def __init__(self, MeasurementConfig):
        TwoTables.__init__(self,MeasurementConfig)

    def GetPosition(self):
        return self.Diffractometer.GetPosition("tth",DEGREE)

    def MeasurementFinished(self):
        if self.TThetaBegin < self.TThetaEnd:
            return self.PositionActual >= self.TThetaEnd
        elif self.TThetaBegin > self.TThetaEnd:
            return self.PositionActual <= self.TThetaEnd

    
    def MoveToNextPoint(self):
        self.PositionTarget += self.TThetaStep
        self.SetTableVelocity("tth",self.TThetaStep)
        self.SetTableVelocity("th",self.TThetaStep/2)
        self.Diffractometer.MoveTo("tth",self.PositionTarget)
        self.Diffractometer.MoveTo("th",self.PositionTarget/2.0)
        self.WaitUntilMovementFinished()

class ThetaTheta(TwoTables):
    def __init__(self, MeasurementConfig):
        TwoTables.__init__(self,MeasurementConfig)

    def GetPosition(self):
        return self.Diffractometer.GetPosition("tth",DEGREE)

    def MeasurementFinished(self):
        if self.TThetaBegin < self.TThetaEnd:
            return self.PositionActual >= self.TThetaEnd
        elif self.TThetaBegin > self.TThetaEnd:
            return self.PositionActual <= self.TThetaEnd


    def MoveToNextPoint(self):
        self.PositionTarget += self.TThetaStep
        self.SetTableVelocity("tth",self.TThetaStep)
        self.SetTableVelocity("th",self.TThetaStep)
        self.Diffractometer.MoveTo("tth",self.PositionTarget)
        self.Diffractometer.MoveTo("th",self.PositionTarget)
        self.WaitUntilMovementFinished()

class Theta(OneTable):
    def __init__(self, MeasurementConfig):
        OneTable.__init__(self,MeasurementConfig)

    def InitializeTables(self):
        self.Diffractometer.SetTableTolerance("th",self.ThetaAccuracy)
        actualpos = self.Diffractometer.GetPosition("th")
        step = abs(actualpos-self.ThetaBegin)
        self.SetTableVelocity("th",step)
        self.PositionTarget = self.ThetaBegin
        self.Diffractometer.MoveTo("th",self.ThetaBegin)
        self.WaitUntilMovementFinished()

    def GetPosition(self):
        return self.Diffractometer.GetPosition("th",DEGREE)

    def MeasurementFinished(self):
        if self.ThetaBegin < self.ThetaEnd:
            return self.PositionActual >= self.ThetaEnd
        elif self.ThetaBegin > self.ThetaEnd:
            return self.PositionActual <= self.ThetaEnd

    def WaitUntilMovementFinished(self):
        while self.Diffractometer.GetTableMovement("th"):
##             if self._want_abort:
##                 self.StopTables()
##                 return
            time.sleep(0.5)
            pass

    def FinalizeTables(self):
        actualpos = self.Diffractometer.GetPosition("th")
        step = abs(actualpos-self.ThetaBegin)
        self.SetTableVelocity("th",step)
        self.Diffractometer.MoveTo("th",self.ThetaBegin)

        self.WaitUntilMovementFinished()




    def MoveToNextPoint(self):
        self.PositionTarget += self.ThetaStep
        self.SetTableVelocity("th",self.ThetaStep)
        self.Diffractometer.MoveTo("th",self.PositionTarget)
        self.WaitUntilMovementFinished()

    def StopTables(self):
        self.Diffractometer.StopTable("th")


class TTheta(OneTable):
    def __init__(self, MeasurementConfig):
        OneTable.__init__(self,MeasurementConfig)
        
    def InitializeTables(self):
        self.Diffractometer.SetTableTolerance("tth",self.TthetaAccuracy)
        actualpos = self.Diffractometer.GetPosition("tth")
        step = abs(actualpos-self.TThetaBegin)
        self.SetTableVelocity("tth",step)
        self.PositionTarget = self.TThetaBegin
        self.Diffractometer.MoveTo("tth",self.TThetaBegin)
        self.WaitUntilMovementFinished()
        melogger.info("InitializeTables TTheta")
 
    def GetPosition(self):
        return self.Diffractometer.GetPosition("tth",DEGREE)

    def MeasurementFinished(self):
        if self.TThetaBegin < self.TThetaEnd:
            return self.PositionActual >= self.TThetaEnd
        elif self.TThetaBegin > self.TThetaEnd:
            return self.PositionActual <= self.TThetaEnd

    def WaitUntilMovementFinished(self):
        while self.Diffractometer.GetTableMovement("tth"):
##             if self._want_abort:
##                 self.StopTables()
##                 return
            time.sleep(0.5)
            pass


    def MoveToNextPoint(self):
        self.PositionTarget += self.TThetaStep
        self.SetTableVelocity("th",self.TThetaStep)
        self.Diffractometer.MoveTo("tth",self.PositionTarget)
        self.WaitUntilMovementFinished()
        
    def StopTables(self):
        self.Diffractometer.StopTable("tth")


    def FinalizeTables(self):
        actualpos = self.Diffractometer.GetPosition("tth")
        step = abs(actualpos-self.TThetaBegin)
        self.SetTableVelocity("tth",step)
        self.Diffractometer.MoveTo("tth",self.TThetaBegin)




class ScanMode(object):
    """Class to difene the ScanMode. It must be subclassed.
    """
    def __init__(self, MeasurementConfig):
        self.HolderSpeed = MeasurementConfig["sampleholder"]["holderspeed"]
    
    def SpinSampleHolder(self):
        self.Diffractometer.SpinSampleHolder(self.HolderSpeed)

    def StopSampleHolder(self):
        self.Diffractometer.StopSampleHolder()

    def StartSampleHolder(self):
        self.ActiveSampleHolder()
        self.SpinSampleHolder()

class DebyeScherrer(ScanMode):
    def __init__(self, MeasurementConfig):
        ScanMode.__init__(self,MeasurementConfig)
    
    def ActiveSampleHolder(self):
        self.Diffractometer.ActiveSampleHolder("capilar")

class Reflection(ScanMode):
    def __init__(self, MeasurementConfig):
        ScanMode.__init__(self,MeasurementConfig)

    def ActiveSampleHolder(self):
        self.Diffractometer.ActiveSampleHolder("flat")


class DetectionType(object):
    pass

class TimeDetectionType(DetectionType):
    """Measurement in constant time mode."""

    def __init__(self, MeasurementConfig):
        self.DetectionType = TIMEMODE
        self.DetectionTypeNumber = MeasurementConfig["range"]["timestep"]

    def SetDetectorMode(self):
        self.Diffractometer.ProgModoDet("time")
        self.Diffractometer.ProgDet(self.DetectionTypeNumber)

class CountsDetectionType(DetectionType):
    """Measurement in constant pulse mode."""

    def __init__(self, MeasurementConfig):
        self.DetectionType = PULSEMODE
        self.DetectionTypeNumber = MeasurementConfig["range"]["countsstep"]

    def SetDetectorMode(self):
        self.Diffractometer.ProgModoDet("pulse")
        self.Diffractometer.ProgDet(self.DetectionTypeNumber)


def GetMeasurementClass(notify_window, MeasurementConfig,ScanModeTypeDetection = None):
    """ Build the Measurement Class. Depends on the options in
    ScanModeInfo.
    """

    ScanModeInfo = MeasurementConfig["scanmode"]["mode"]
    ScanTypeInfo = MeasurementConfig["scanmode"]["type"]
    DetectionTypeInfo = MeasurementConfig["scanmode"]["detectiontype"]

##     if ScanModeTypeDetection != None:
##         ScanModeInfo = ScanModeTypeDetection[0]
##         ScanTypeInfo = ScanModeTypeDetection[1]
##         DetectionTypeInfo = ScanModeTypeDetection[2]


    if ScanModeInfo == "Debye-Scherrer":
        ScanModeBase = DebyeScherrer
    elif ScanModeInfo == "Reflection":
        ScanModeBase = Reflection
    else:
        ErrorDlg.ErrorMessage(notify_window, "Bad ScanMode")        


    if ScanTypeInfo == "Theta: 2 Theta":
        ScanTypeBase = TThetaTheta
    elif ScanTypeInfo == "Theta: Theta":
        ScanTypeBase = ThetaTheta
    elif ScanTypeInfo == "2 Theta":
        ScanTypeBase = TTheta
    elif ScanTypeInfo == "Theta": 
        ScanTypeBase = Theta
    else:
        ErrorDlg.ErrorMessage(notify_window, "Bad ScanType")        

    if DetectionTypeInfo == "Time":
        DetectionTypeBase = TimeDetectionType        
    elif DetectionTypeInfo == "Counts":
        DetectionTypeBase = CountsDetectionType
    else:
        ErrorDlg.ErrorMessage(notify_window, "Bad DetectionType")        

    Bases = (MeasurementBasic,ScanModeBase,ScanTypeBase,DetectionTypeBase)


    # type builds a new class, using Bases as a tuple of base classes.
    newclass = type("MeasurementClass",Bases,{})


    # The following function will be the __init__ of newclass
    def newclassinit(self, MeasurementConfig):
        # We have to iterate over the bases of newclass. newclass is
        # sublcases of
        for base in self.__class__.__bases__[0].__bases__:
            base.__init__(self, MeasurementConfig)

    setattr(newclass, "__init__", newclassinit)

    class newclassthread(newclass,threading.Thread):
        def __init__(self,notify_window,MeasurementConfig):
            threading.Thread.__init__(self,target=self.startmeasurement)
            self._exception = None
            newclass.__init__(self,MeasurementConfig)
            self._notify_window = notify_window
            self._working = False 
            self._want_abort = False
            self.Layers = []
            self.traceback_queue = Queue.Queue()            

        def startmeasurement(self):
            try:
                self._working = True
                if hasattr(self._notify_window,'timer'):
                    self._notify_window.timer.Start(1000)
                self.Measure()
                self._working = False
                self.StopTables()
                self.FinalizeMeasurement()
                wx.PostEvent(self._notify_window, FinishMeasurementEvent())
            except:
                exception = sys.exc_info()
                traceback.print_tb(exception[2])
                self.traceback_queue.put(exception)
                wx.PostEvent(self._notify_window,
                            RaiseExceptionEvent(exception))


        def abort(self):
            melogger.info("Measurement aborted")
            self._want_abort = True
##            if hasattr(self._notify_window,'timer'):
##                self._notify_window.timer.Stop()

            if self.Diffractometer.GetStateMeasurement() == True:
                self.Diffractometer.StopMeasure()
##            self.StopTables()
##            self.FinalizeMeasurement()
            self._working = False
            wx.PostEvent(self._notify_window, FinishMeasurementEvent())

    MeasureObject = newclassthread(notify_window,MeasurementConfig)

    return MeasureObject


