#----------------------------------------------------------------------------
# Name:         AboutDlg.py
# Author:       XXXX
# Created:      XX/XX/XX
# Copyright:    
#----------------------------------------------------------------------------

#from wxPython.wx import *
from control_wdr import *
import wx
from Globals import loadimage

# WDR: classes

class AboutDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        

        BigIconBitmap = wx.Image(loadimage("bigicon-t.png"),wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        BigIcon = wx.StaticBitmap(self,ID_BIGICON_FOREIGN,BigIconBitmap,wx.DefaultPosition,
                                 (BigIconBitmap.GetWidth(),
                                  BigIconBitmap.GetHeight()))
        AboutDialogFunc( self, True )

        # WDR: handler declarations for AboutDlg.py

    # WDR: methods for AboutDlg.py

    # WDR: handler implementations for AboutDlg.py


