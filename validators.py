import wx

# The reason for the DataHolder class has been explainded by Robin Dunn
# in the following email in the wxPython-users mailing list
#
# String objects in Python are immutable.  That means that when you do
#
#        self.data = self.GetWindow().GetValue()
#
# self.data is now a reference to a *new* string, and the original string 
# is still refered to by the original variable.  (In C++ data objects are 
# not immutable and so they can be changed in place, which is why the 
# validator concept works better there.)
#
# One way to work around this is to make a data holder class, and pass 
# instances of this class to your validator.

# [snip class definitions ...]

# Now, since the self.holder is not being reassigned (so it still refers 
# to the original object) then the change of data in the validator will 
# still be accessible via the original parent.GeneratorVoltage instance
# of the holder.


class DataHolder(object):
    """ Data holder for use with validators. """
    def __init__(self, value=None):
        self.data = value

    def __str__(self):
        return str(self.data)

class GenericValidator(wx.PyValidator):
    """ A generic validator. Ideally it will be subclassed to especify the
    vehaviour of self.Validate() and so own.
    Initialization variables:
        * holder -- A instance of DataHolder with the data to be
        validated.
    """
    def __init__(self, holder):
        wx.PyValidator.__init__(self)
        self.holder = holder

    def Clone(self):
        return GenericValidator(self.holder)

    def Validate(self, window):
        return True

    def TransferToWindow(self):
        self.GetWindow().SetValue(self.holder.data)
        return True

    def TransferFromWindow(self):
        self.holder.data = self.GetWindow().GetValue()
        return True

class RadioboxValidator(wx.PyValidator):
    """Validator for Radiobox."""

    def __init__(self, holder):
        wx.PyValidator.__init__(self)
        self.holder = holder

    def Clone(self):
        return RadioboxValidator(self.holder)

    def Validate(self, window):
        return True

    def TransferToWindow(self):
        self.GetWindow().SetStringSelection(self.holder.data)
        return True

    def TransferFromWindow(self):
        self.holder.data = self.GetWindow().GetStringSelection()
        return True

class IntegerTextValidator(GenericValidator):
    """Validator for TextCtrls whit integer values."""

    def __init__(self, holder):
        GenericValidator.__init__(self, holder)

    def Clone(self):
        return IntegerTextValidator(self.holder)

    def Validate(self, window):
        win = self.GetWindow()
        value = win.GetValue()
        try:
            int(value)
            return True
        except ValueError:
            name = win.GetName()
            message = "%s invalid value.\n%s must be an integer."\
                      %(str(value),name)
            wx.MessageBox(message,"Error")
            return False
            
    def TransferToWindow(self):
        value = self.holder.data
        if value == None:
            self.GetWindow().SetValue("")
        else:
            self.GetWindow().SetValue(str(self.holder.data))
        return True

    def TransferFromWindow(self):
         self.holder.data = int(self.GetWindow().GetValue())
         return True

class FloatTextValidator(GenericValidator):
    """Validator for TextCtrls whit float values."""

    def __init__(self, holder):
        GenericValidator.__init__(self, holder)

    def Clone(self):
        return FloatTextValidator(self.holder)

    def Validate(self, window):
        win = self.GetWindow()
        value = win.GetValue()
        try:
            float(value)
            return True
        except ValueError:
            name = win.GetName()
            message = "%s invalid value.\n%s must be an integer."\
                      %(str(value),name)
            wx.MessageBox(message,"Error")
            return False
            
    def TransferToWindow(self):
        value = self.holder.data
        if value == None:
            self.GetWindow().SetValue("")
        else:
            self.GetWindow().SetValue(str(self.holder.data))
        return True

    def TransferFromWindow(self):
         self.holder.data = float(self.GetWindow().GetValue())
         return True

class IntegerValidator(GenericValidator):
    """Validator for controls with integer values."""

    def __init__(self, holder):
        GenericValidator.__init__(self, holder)

    def Clone(self):
        return IntegerValidator(self.holder)

    def Validate(self, window):
        win = self.GetWindow()
        value = win.GetValue()
        try:
            int(value)
            return True
        except ValueError:
            name = win.GetName()
            message = "%s invalid value.\n%s must be an integer."\
                      %(str(value),name)
            wx.MessageBox(message,"Error")
            return False
            
    def TransferToWindow(self):
        value = self.holder.data
        if value == None:
            self.GetWindow().SetValue(0)
        else:
            self.GetWindow().SetValue(int(self.holder.data))
        return True

    def TransferFromWindow(self):
         self.holder.data = int(self.GetWindow().GetValue())
         return True

class BooleanValidator(GenericValidator):
    """Validator for controls with boolean values."""

    def __init__(self, holder):
        GenericValidator.__init__(self, holder)

    def Clone(self):
        return BooleanValidator(self.holder)

    def Validate(self, window):
        win = self.GetWindow()
        value = win.GetValue()
        try:
            isinstance(value,str)
            return True
        except ValueError:
            name = win.GetName()
            message = "%s invalid value.\n%s must be a string."\
                      %(str(value),name)
            wx.MessageBox(message,"Error")
            return False
            
    def TransferToWindow(self):
        value = self.holder.data
        if value == None:
            self.GetWindow().SetValue(0)
        else:
            self.GetWindow().SetValue(eval(self.holder.data))
        return True

    def TransferFromWindow(self):
         self.holder.data = str(self.GetWindow().GetValue())
         return True

def SetValidatorsToWidgets(dialog,WidgetName,WidgetType,ValidatorType):
    """ Set the validator of type ValidatorType to the widget
    WidgetName of Tpye WidgetType. The validator is bounded to the
    variable with name WidgetName, wich is an attribute of
    widget or of the parent of widget."""

    # GetterFucntionName is the getter of the dialog that return the widget
    GetterFunctionName = 'Get'+WidgetName+WidgetType
    ValidatorName = ValidatorType+'Validator'
    getter = dialog.__getattribute__(GetterFunctionName)
    widget = getter()
    validator = eval(ValidatorName)
    # variable is the variable wich the validator is related.  First
    # the variable is searched in dialog. If the variable is not in
    # widget it is seached in parent.
    if WidgetName in dir(dialog):
        variable = dialog.__getattribute__(WidgetName)
    else:
        parent = dialog.GetParent()
        variable = parent.__getattribute__(WidgetName)
    widget.SetValidator(validator(variable))

def SetDataHolder(dialog,WidgetName,data):
    """Set a DataHolder of name WidgetName in dialog."""
    dataholder = DataHolder(data)
    dialog.__dict__[WidgetName] = dataholder

