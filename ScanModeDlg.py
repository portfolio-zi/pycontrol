from control_wdr import *
import wx

from validators import SetValidatorsToWidgets

class ScanModeDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        ScanModeDlgFunc( self, True )
        

        # Get default values
        self.CurrentConfig = parent.CurrentConfig


        self.values = {}
        self.values['ScanModeMode'] = ["scanmode","mode",'Combo','Generic']
        self.values['ScanModeType'] = ["scanmode","type",'Combo','Generic']
        self.values['ScanModeDetectionType'] = ["scanmode","detectiontype",'Radiobox','Radiobox']


        # Put default values in the dialog.
        for WidgetName in self.values.keys():
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)


        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)


    #----------------------------------------    
    # Getters 
    #----------------------------------------    

    def GetScanModeModeCombo(self):
        return self.FindWindowById(ID_SCANMODE_MODE_COMBO)

    def GetScanModeTypeCombo(self):
        return self.FindWindowById(ID_SCANMODE_TYPE_COMBO)

    def GetScanModeDetectionTypeRadiobox(self):
        return self.FindWindowById(ID_SCANMODE_DETECTIONTYPE_RADIOBOX)



    def OnOK(self,event):
        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                value = str(eval('self.GetParent().'+item[0]+'.data'))
                self.CurrentConfig[section][option] = value
            #self.dfconf.save()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return
