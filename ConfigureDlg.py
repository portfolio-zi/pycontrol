from control_wdr import *
import wx

from validators import SetValidatorsToWidgets, SetDataHolder

from constant import *
from wx.lib.masked import IpAddrCtrl

class ConfigureDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
                 style = wx.DEFAULT_DIALOG_STYLE):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)

        self.SetExtraStyle(wx.WS_EX_VALIDATE_RECURSIVELY )

        ConfigureDlgFunc( self, True )

        notebook = self.FindWindowById(ID_CONFIGURATION_NOTEBOOK)

        panel = wx.Panel(notebook,-1,pos = wx.DefaultPosition, size = wx.Size(50,50))

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox = wx.BoxSizer(wx.VERTICAL)
        #hbox.Add(1,1,1)
        grid = wx.GridSizer(2,2,10,0)
        self.IPAddressCtrl = IpAddrCtrl(panel,-1, style = wx.TE_PROCESS_TAB)
        self.IPAddressCtrl.SetToolTip(wx.ToolTip("0...255"))
        IPtext = wx.StaticText(panel,-1,"IP")
        PortText1 = wx.StaticText(panel,-1,"Port")
        self.IPPortSpiner = wx.SpinCtrl(panel,-1,"")
        self.IPPortSpiner.SetRange(0,65535)
        self.IPPortSpiner.SetToolTip(wx.ToolTip("0,...,65535"))
        grid.AddMany([IPtext,self.IPAddressCtrl,PortText1,self.IPPortSpiner])
        hbox.Add(grid,1,wx.ALIGN_TOP|wx.TOP)
        vbox.Add(hbox,1,wx.ALIGN_CENTER|wx.ALL,border=20)
                 

        
        panel.SetSizer(vbox)
        panel.SetAutoLayout(True)
        vbox.Fit(panel)

        notebook.AddPage(panel,"IP")

        # Configuration object
        self.DefaultConfig = self.GetParent().DefaultConfig
        self.values = {}
        
        self.Bind(wx.EVT_BUTTON,self.OnOK,id=wx.ID_OK)

        # Get default values

        
        # Generator

        self.values['GeneratorDefaultmA'] = ['generator','current','TextCtrl','IntegerText',None]
        self.values['GeneratorDefaultKv'] = ['generator','voltage','TextCtrl','IntegerText',None]
        self.values['GeneratorShutdown'] = ['generator','shutdown','CheckBox','Boolean',None]
        self.values['GeneratorShutter'] = ['generator','opencloseshutter','CheckBox','Boolean',None]
        self.values['GeneratorNotXray'] = ['generator','notxray','CheckBox','Boolean',None]
        self.values['GeneratorMaximumKv'] = [None,GENVOLMAX,'TextCtrl','IntegerText',None]
        self.values['GeneratorMaximummA'] = [None,GENCURMAX,'TextCtrl','IntegerText',None]
        self.values['GeneratorMaximumPower'] = [None,GENPOWMAX,'TextCtrl','IntegerText',None]
        


        # Disable maximum TextCtrl
        self.GetGeneratorMaximumKvTextCtrl().Enable(False)
        self.GetGeneratorMaximummATextCtrl().Enable(False)
        self.GetGeneratorMaximumPowerTextCtrl().Enable(False)

        self.GetGeneratorMaximumKvTextCtrl().Enable(False)

        # Detector

        self.values['DetectorHV'] = ['detector','highvoltage','Slider','Generic','int']
        self.values['DetectorPolarity'] = ['detector','polarity','Combo','Generic',None]
        self.values['DetectorTimeCte'] = ['detector','tcte','Combo','Generic',None]
        self.values['DetectorGain'] = ['detector','gain','TextCtrl','IntegerText',None]
        self.values['DetectorMode'] = ['detector','mode','Combo','Generic',None]
        self.values['DetectorLL'] = ['detector','lowerlevel','TextCtrl','FloatText',None]
        self.values['DetectorUL'] = ['detector','upperlevel','TextCtrl','FloatText',None]



        # Sample Holder

        self.values['HolderType'] = ['sampleholder','type','Combo','Generic',None]
        self.values['HolderVelocity'] = ['sampleholder','holderspeed','TextCtrl','FloatText',None]
        self.values['IsHolder'] = ['sampleholder','isholder','CheckBox','Boolean',None]
        
        
        # Tables

        self.values['TablesTthetaVelocity'] = ['tables','tth-speed','Slider','Generic','int']
        self.values['TablesThetaVelocity'] = ['tables','th-speed','Slider','Generic','int']
        self.values['TablesGoHome'] = ['tables','go-home','CheckBox','Boolean',None]
        self.values['TablesTthetaAccuracy'] = ['tables',"tth-accuracy",'TextCtrl','FloatText',None]
        self.values['TablesThetaAccuracy'] = ['tables',"th-accuracy",'TextCtrl','FloatText',None]
        


        # Range

        self.values['TthetaBegin'] = ['range','2thetabegin','TextCtrl','FloatText',None]
        self.values['TthetaEnd'] = ['range','2thetaend','TextCtrl','FloatText',None]
        self.values['TthetaStep'] = ['range','2thetastep','TextCtrl','FloatText',None]
        self.values['ThetaBegin'] = ['range','thetabegin','TextCtrl','FloatText',None]
        self.values['ThetaEnd'] = ['range','thetaend','TextCtrl','FloatText',None]
        self.values['ThetaStep'] = ['range','thetastep','TextCtrl','FloatText',None]
        self.values['TimeStep'] = ['range','timestep','TextCtrl','Generic',None]
        self.values['CountsStep'] = ['range','countsstep','TextCtrl','Generic',None]



        # Scan Mode

        self.values['ScanMode'] = ['scanmode','mode','Combo','Generic',None]
        self.values['ScanType'] = ['scanmode','type','Combo','Generic',None]
        self.values['DetectionType'] = ['scanmode','detectiontype','Radiobox','Radiobox',None]



        # Comunication
        self.values["IPAddress"] = ["comunication","ip",'Ctrl','Generic',None]
        self.values["IPPort"] = ["comunication","port",'Spiner','Integer',None]



        # Put default values in the dialog.
        for WidgetName in self.values.keys():
	    section = self.values[WidgetName][0]
            entry = self.values[WidgetName][1]
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            cast = self.values[WidgetName][4]

            if section != None and entry != None:
                data = self.DefaultConfig[section][entry]
            elif section == None:
                data = entry
            if cast != None:
                cast = eval(cast)
                data = cast(data)
            SetDataHolder(self,WidgetName,data)
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)

        
    #----------------------------------------
    # Getters
    #----------------------------------------

    # Generato
    def GetGeneratorMaximumKvTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_MAXIMUM_KV_TEXTCTRL)
    def GetGeneratorMaximummATextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_MAXIMUM_MA_TEXTCTRL)
    def GetGeneratorMaximumPowerTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_MAXIMUM_POWER_TEXTCTRL)
    def GetGeneratorDefaultKvTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_DEFAULT_KV_TEXTCTRL)
    def GetGeneratorDefaultmATextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_DEFAULT_MA_TEXTCTRL)
    def GetGeneratorShutdownCheckBox(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_SHUTDOWN_CHECKBOX)
    def GetGeneratorShutterCheckBox(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_OPENCLOSESHUTTER_CHECKBOX)
    def GetGeneratorNotXrayCheckBox(self):
        return self.FindWindowById(ID_CONFIGURE_GENERATOR_DEBUGXRAY_CHECKBOX)

    # Detector
    def GetDetectorHVSlider(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_HV_SLIDER)
    def GetDetectorPolarityCombo(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_POLARITY_COMBO)
    def GetDetectorTimeCteCombo(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_TIMECTE_COMBO)
    def GetDetectorGainTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_GAIN_TEXTCTRL)
    def GetDetectorModeCombo(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_MODE_COMBO)    
    def GetDetectorLLTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_LL_TEXTCTRL)
    def GetDetectorULTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_DETECTOR_UL_TEXTCTRL)

    # Sample Holder
    def GetHolderTypeCombo(self):
        return self.FindWindowById(ID_CONFIGURE_HOLDER_TYPE_COMBO)
    def GetHolderVelocityTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_HOLDER_VELOCITY_TEXTCTRL)
    def GetIsHolderCheckBox(self):
        return self.FindWindowById(ID_CONFIGURE_HOLDER_ISSAMPLE_CHECKBOX)

    # Tables
    def GetTablesTthetaVelocitySlider(self):
        return self.FindWindowById(ID_CONFIGURE_TABLES_TTHETA_VELOCITY_SLIDER)
    def GetTablesThetaVelocitySlider(self):
        return self.FindWindowById(ID_CONFIGURE_TABLES_THETA_VELOCITY_SLIDER)
    def GetTablesGoHomeCheckBox(self):
        return self.FindWindowById(ID_CONFIGURE_TABLES_GOHOME_CHECKBOX)

    # ranges
    def GetTthetaBeginTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_TTHETA_BEGIN_TEXTCTRL)
    def GetTthetaEndTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_TTHETA_END_TEXTCTRL)
    def GetTthetaStepTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_TTHETA_STEP_TEXTCTRL)
    def GetThetaBeginTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_THETA_BEGIN_TEXTCTRL)
    def GetThetaEndTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_THETA_END_TEXTCTRL)
    def GetThetaStepTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_THETA_STEP_TEXTCTRL)
    def GetTimeStepTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_TIMESTEP_TEXTCTRL)
    def GetCountsStepTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_RANGES_COUNTSSTEP_TEXTCTRL)
    def GetTablesTthetaAccuracyTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_TABLES_TTHACCURACY_TEXTCTRL)
    def GetTablesThetaAccuracyTextCtrl(self):
        return self.FindWindowById(ID_CONFIGURE_TABLES_THACCURACY_TEXTCTRL)
    
    # Scan Mode
    def GetScanModeCombo(self):
        return self.FindWindowById(ID_CONFIGURE_SCANMODE_MODE_COMBO)
    def GetScanTypeCombo(self):
        return self.FindWindowById(ID_CONFIGURE_SCANMODE_TYPE_COMBO)
    def GetDetectionTypeRadiobox(self):
        return self.FindWindowById(ID_CONFIGURE_SCANMODE_DETECTIONTYPE_RADIOBOX)
    # IP
    def GetIPAddressCtrl(self):
        return self.IPAddressCtrl
    def GetIPPortSpiner(self):
        return self.IPPortSpiner

    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnOK(self,event):
        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                if section == None:
                    # This value must be not validate!
                    continue
                    
                value = eval('self.'+item[0])
                print section, option
                self.DefaultConfig[section][option] = str(value)
            self.DefaultConfig.write()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return
            
