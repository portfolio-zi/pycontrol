from comunicacion import diffracsocket
from datetime import datetime

from timeoutsocket import Timeout
import time

Diff = diffracsocket("158.227.49.160",1020)


minangle = 0
maxangle = 50

steps = [1,0.1,0.01,0.001,0.0001]#,0.00001]
velocities = [10,30,70,100]
#steps = [5]
velocities = [100]
stepn = 20
tolerances = [0.00006,0.0001,0.001,0.01]


modes = ['tth','th','thtth']
#modes = ['thtth2']


def WaitUntilMovementFinishedtth():
    test = True
    while test:
        try:
            test = Diff.GetTableMovement("tth")
        except Timeout:
            print "exception" in "Wait"
            test = True
        time.sleep(0.5)

def WaitUntilMovementFinishedth():
    test = True
    while test:
        try:
            test = Diff.GetTableMovement("th")
        except Timeout:
            print "exception" in "Wait"
            test = True
        time.sleep(0.5)

def WaitUntilMovementFinishedthtth():
    test = True
    while test:
        try:
            test1 = Diff.GetTableMovement("th")
            test2 = Diff.GetTableMovement("tht")
        except Timeout:
            print "exception" in "Wait"
            test = test1 or test2
        time.sleep(0.5)



def MoveToNextPointtth(actual,step):
    PositionTarget = actual+step
    try:
        Diff.MoveTo("tth",PositionTarget)
    except Timeout:
        print "exception" in "Move"
        pass
    WaitUntilMovementFinishedtth()

def MoveToNextPointth(actual,step):
    PositionTarget = actual+step
    try:
        Diff.MoveTo("th",PositionTarget)
    except Timeout:
        print "exception" in "Move"
        pass
    WaitUntilMovementFinishedth()


def MoveToNextPointthtth(actual,step):
    PositionTarget = actual+step
    try:
        Diff.MoveTo("tth",PositionTarget)
        Diff.MoveTo("th",PositionTarget/2.0)
    except Timeout:
        print "exception" in "Move"
        pass
    WaitUntilMovementFinishedtth()

def MoveToNextPointthtth2(actual,step):
    PositionTarget = actual+step
    try:
        Diff.MoveTo("tth",PositionTarget)
        Diff.MoveTo("th",PositionTarget)
    except Timeout:
        print "exception" in "Move"
        pass
    WaitUntilMovementFinishedtth()
    Diff.OpenShutter()
    Diff.CloseShutter()


functions = {"MoveToNextPointth":MoveToNextPointth,
             "MoveToNextPointtth":MoveToNextPointtth,
             "MoveToNextPointthtth2":MoveToNextPointthtth2,
             "MoveToNextPointthtth":MoveToNextPointthtth}


def ProcessStep(mode,step):
    print "> Step = %f Stepnumbers = %i" % (step,stepn)
    actual = minangle
    totals = 0
    for i in range(stepn):
        movefuncname = "MoveToNextPoint"+mode
        beforetime = datetime.now()
        f = functions[movefuncname]
#        print ">>", actual,step
        f(actual,step)
        actual += step
        aftertime = datetime.now()
        duration = aftertime-beforetime
        secs = duration.seconds
        totals += secs
    media = float(totals)/stepn
    print "Media %f" % (media)
    f(0,0)


def SetVelocity(mode,velocity):
    if mode == 'th':
        Diff.TableVelocity('th',velocity)
    if mode == 'tth':
        Diff.TableVelocity('tth',velocity)
    if mode == 'thtth':
        Diff.TableVelocity('th',velocity)
        Diff.TableVelocity('tth',velocity)


def ProcessVelocity(mode,velocity):
    print "* Velocity  = %f" % (velocity)
    SetVelocity(mode,velocity)
    for step in steps:
        ProcessStep(mode,step)
    

def ProcessTolerance(t,mode):
    print "Tolerance %f" % (t)
    Diff.SetTableTolerance('th',t)
    Diff.SetTableTolerance('tth',t)
    for velocity in velocities:
        ProcessVelocity(mode,velocity)

def ProcessMode(mode):
    print "** Mode %s" % (mode.upper())
    for t in tolerances:
        ProcessTolerance(t,mode)
    print




def main():
    for mode in modes:
        ProcessMode(mode)


if __name__ == '__main__':
    main()
