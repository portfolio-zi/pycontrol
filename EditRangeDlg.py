from control_wdr import *
import wx

from validators import SetValidatorsToWidgets

class EditRangeDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        EditRangeDlgFunc( self, True )

        self.parent = parent
        self.values = {}

        # Get default values
        self.CurrentConfig = parent.CurrentConfig
        
        # Put default values in the dialog.

        self.values['EditRange2ThetaBegin'] = ['range','2thetabegin','TextCtrl','FloatText']
        self.values['EditRange2ThetaEnd'] = ['range','2thetaend','TextCtrl','FloatText']
        self.values['EditRange2ThetaStep'] = ['range','2thetastep','TextCtrl','FloatText']
        self.values['EditRangeThetaBegin'] = ['range','thetabegin','TextCtrl','FloatText']
        self.values['EditRangeThetaEnd'] = ['range','thetaend','TextCtrl','FloatText']
        self.values['EditRangeThetaStep'] = ['range','thetastep','TextCtrl','FloatText']
        self.values['EditRangeTimeStep'] = ['range','timestep','TextCtrl','FloatText']
        self.values['EditRangeCountsStep'] = ['range','countsstep','TextCtrl','FloatText']

        # Put default values in the dialog.
        for WidgetName in self.values.keys():
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)


        # Enable/Disable the range textctrs when theta and 2theta are
        # cupled or only one table is to move
        if parent.ScanModeType.data in ("Theta: 2 Theta",
                                        "Theta: Theta"):
            self.GetEditRangeThetaBeginTextCtrl().Enable(False)
            self.GetEditRangeThetaEndTextCtrl().Enable(False)
            self.GetEditRangeThetaStepTextCtrl().Enable(False)
        elif parent.ScanModeType.data == ("Theta"):
            self.GetEditRange2ThetaBeginTextCtrl().Enable(False)
            self.GetEditRange2ThetaEndTextCtrl().Enable(False)
            self.GetEditRange2ThetaStepTextCtrl().Enable(False)
        elif parent.ScanModeType.data == ("2 Theta"):
            self.GetEditRangeThetaBeginTextCtrl().Enable(False)
            self.GetEditRangeThetaEndTextCtrl().Enable(False)
            self.GetEditRangeThetaStepTextCtrl().Enable(False)

    

        # Enable/Disable the DetectionType textctrls
        if parent.ScanModeDetectionType.data == "Time":
            self.GetEditRangeTimeStepTextCtrl().Enable(True)
            self.GetEditRangeCountsStepTextCtrl().Enable(False)
        elif parent.ScanModeDetectionType.data == "Counts":
            self.GetEditRangeTimeStepTextCtrl().Enable(False)
            self.GetEditRangeCountsStepTextCtrl().Enable(True)
        else:
            pass

        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)

    #----------------------------------------    
    # Getters 
    #----------------------------------------    

    def GetEditRange2ThetaBeginTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_2THETABEGIN_TEXTCTRL)

    def GetEditRange2ThetaEndTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_2THETAEND_TEXTCTRL)

    def GetEditRange2ThetaStepTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_2THETASTEP_TEXTCTRL)

    def GetEditRangeThetaBeginTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_THETABEGIN_TEXTCTRL)

    def GetEditRangeThetaEndTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_THETAEND_TEXTCTRL)

    def GetEditRangeThetaStepTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_THETASTEP_TEXTCTRL)

    def GetEditRangeTimeStepTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_TIMESTEP_TEXTCTRL)

    def GetEditRangeCountsStepTextCtrl(self):
        return self.FindWindowById(ID_EDITRANGE_COUNTSSTEP_TEXTCTRL)


    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnOK(self,event):
        # Calculate the angle when movement are coupled
        if self.parent.ScanModeType.data == "Theta: 2 Theta":

            TThetaBegin = float(self.GetEditRange2ThetaBeginTextCtrl().GetValue())
            TThetaEnd = float(self.GetEditRange2ThetaEndTextCtrl().GetValue())
            TThetaStep = float(self.GetEditRange2ThetaStepTextCtrl().GetValue())

            ThetaBegin = TThetaBegin/2
            ThetaEnd = TThetaEnd/2
            ThetaStep = TThetaStep/2

            self.GetEditRangeThetaBeginTextCtrl().SetValue(str(ThetaBegin))
            self.GetEditRangeThetaEndTextCtrl().SetValue(str(ThetaEnd))
            self.GetEditRangeThetaStepTextCtrl().SetValue(str(ThetaStep))



        elif self.parent.ScanModeType.data == "Theta: Theta":
            TThetaBegin = float(self.GetEditRange2ThetaBeginTextCtrl().GetValue())
            TThetaEnd = float(self.GetEditRange2ThetaEndTextCtrl().GetValue())
            TThetaStep = float(self.GetEditRange2ThetaStepTextCtrl().GetValue())
            ThetaBegin = TThetaBegin
            ThetaEnd = TThetaEnd
            ThetaStep = TThetaStep

            self.GetEditRangeThetaBeginTextCtrl().SetValue(str(ThetaBegin))
            self.GetEditRangeThetaEndTextCtrl().SetValue(str(ThetaEnd))
            self.GetEditRangeThetaStepTextCtrl().SetValue(str(ThetaStep))



        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                value = str(eval('self.GetParent().'+item[0]+'.data'))
                self.CurrentConfig[section][option] = value
            #self.dfconf.save()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return


