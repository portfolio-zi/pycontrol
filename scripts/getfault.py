from optparse import OptionParser


parser = OptionParser()
parser.add_option("-f", "--file", dest="file",
                  help="X")
(options, args) = parser.parse_args()

filename = args[0]

datafile = open(filename,"r")


def gethc(line):
    h = -1
    c = -1
    if not line.startswith("Handle") and not line.startswith("Codigo"):
        return (h,c)
    if line.startswith("Handle"):
        l = line.split(" ")
        h = int(l[2])
    if line.startswith("Codigo"):
        l = line.split(" ")
        c = int(l[2])
    return (h,c)




firstline = True
defined = False

for line in datafile:
    handle,code = gethc(line)
    if not firstline:
        if defined:
            if handle == oldh:
                consecutive = True
            else:
                consecutive = False
            if not consecutive:
                if handle == oldh+1:
                    print handle
    if handle != -1 or code != -1:
        defined = True
        oldh = handle
        oldc = code
    firstline = False
