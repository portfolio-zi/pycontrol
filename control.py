#!/usr/local/bin/python
#<-- encoding: iso-8859-1  --->
#import gettext
#gettext.install('control','./locale',unicode=1)

import logging
import logging.handlers

# Logging information
mainlogger = logging.getLogger('mainlog')
loghdlr = logging.handlers.RotatingFileHandler('mainlog.log',
                                               maxBytes=5242880,
                                               backupCount=5)
formatter = logging.Formatter('%(asctime)s %(message)s')
loghdlr.setFormatter(formatter)
mainlogger.addHandler(loghdlr) 
mainlogger.setLevel(logging.DEBUG)


from control_wdr import *
import wx

global UserDlg, DF, AboutDlg, ConfigureDlg

import AboutDlg
import comunicacion
import UserDlg
from configobj import ConfigObj
import DiffractometerFrame as DF
import ConfigureDlg
from Globals import *

import sys
import os.path



#----------------------------------------
# IDs
#----------------------------------------

ID_TOPFRAME = wx.NewId() # Top frame called by wxApp
ID_DIFFRACFRAME = wx.NewId()  # Fram for Diffractometer Control

class MyFrame(wx.Frame):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = (600,650),
        style = wx.DEFAULT_FRAME_STYLE):
        wx.Frame.__init__(self, parent, id, title, pos, size, style)

        self.panel = wx.Panel(self,-1)
        self.CreateMyMenuBar()
        self.CreateMyToolBar()
        self.CreateStatusBar(1)
        #self.SetStatusText("User: %s,dir=%s" %(CURRENTUSER,CURRENTDIR))
        self.SetMyIcon()
        self.SetPannelInformation()
        
        # Configuration objects
        self.DefaultConfig = ConfigObj(loadbasefile("defaultdifrac.cfg"))

        
        # WDR: handler declarations for MyFrame
        self.Bind(wx.EVT_MENU, self.OnAbout, id=ID_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnQuit, id=ID_QUIT)
        self.Bind(wx.EVT_MENU, self.OnMenuUsersNewUser, id=ID_NEWUSER)
        self.Bind(wx.EVT_MENU, self.OnMenuUsersSelectUser, id=ID_SELECTUSER)
        self.Bind(wx.EVT_MENU, self.OnMenuDiffractometerControl,
                  id=ID_DIFFRACTOMETERCONTROL)
        self.Bind(wx.EVT_MENU, self.OnMenuDiffractometerDefault,
                  id=ID_DIFFRACTOMETER_DEFAULT)
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        

        self.timer = wx.PyTimer(self.RefreshonTimer)
        self.timer.Start(1000)
        self.RefreshonTimer()

    # WDR: methods for MyFrame




    def RefreshonTimer(self):
        CURRENTUSER = USERCONF["user-info"]["currentusername"]
        CURRENTDIR = USERCONF["user-info"]["currentdir"]
        self.SetStatusText("User: %s,dir=%s" %(CURRENTUSER,CURRENTDIR))
                
    

    def CreateMyMenuBar(self):
        self.SetMenuBar(CreateMyMenuBarFunc())
    
    def CreateMyToolBar(self):
        tb = self.CreateToolBar(wx.TB_HORIZONTAL|wx.NO_BORDER)
        MyToolBarFunc(tb)

    # WDR: handler implementations for MyFrame
    
    def OnAbout(self, event):
        reload(AboutDlg)
        dlg = AboutDlg.AboutDlg(self,-1,"Dialog")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()
    
    def OnQuit(self, event):
        self.Close(True)
    
    def OnCloseWindow(self, event):
        self.Destroy()


    #----------------------------------------
    # MENU | EJECUTAR
    #----------------------------------------

    #----------------------------------------
    # MENU | USERS
    #----------------------------------------

    def OnMenuUsersNewUser(self, event):
        reload(UserDlg)
        dlg = UserDlg.NewUserDlg(self, -1, "New Users",size=wx.Size(800,800))
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()

    def OnMenuUsersSelectUser(self, event):
        reload(UserDlg)
        dlg = UserDlg.SelectUserDlg(self, -1, "Select User",size=wx.Size(800,800))
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()

    #----------------------------------------
    # MENU | DIFFRACTOMETER
    #----------------------------------------

    def OnMenuDiffractometerControl(self, event):
        """ Calls Difrractometer Control Frame.
        """
        reload(DF)
        frame = DF.DiffractometerFrame(self,#self.FindWindowById(ID_TOPFRAME),
                                      ID_DIFFRACFRAME, #  -1, 
                                       "Diffractometer control")
        frame.Show(True)
##        frame.MakeModal()

        

    def OnMenuDiffractometerDefault(self, event):
        reload(ConfigureDlg)
        dlg = ConfigureDlg.ConfigureDlg(self, -1, "Configure defaults")
        if dlg.ShowModal() == wx.ID_OK:
            pass
        dlg.Destroy()

    #----------------------------------------
    # OTHER METHODS
    #----------------------------------------
    
    def SetMyIcon(self):
        image = wx.Image("icon.xpm", wx.BITMAP_TYPE_XPM)
        image = image.ConvertToBitmap()

        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(image)
        
        self.SetIcon(icon)
        

    def SetPannelInformation(self):
        self.panel.Box = wx.BoxSizer(wx.VERTICAL)

        TextBox = wx.BoxSizer(wx.HORIZONTAL)
        TextImage = wx.Image(loadimage("text-t.png"),wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        TextBitmapBox = wx.StaticBitmap(self.panel,-1,TextImage,wx.DefaultPosition,(TextImage.GetWidth(),TextImage.GetHeight()))
        TextBox.Add(TextBitmapBox,0,wx.ALIGN_CENTER | wx.ALL,5)

        BigIconBox = wx.BoxSizer(wx.HORIZONTAL)
        bigicon = wx.Image(loadimage("bigicon-t.png"), wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        BigIconSizer = wx.StaticBitmap(self.panel, -1, bigicon, wx.DefaultPosition, (bigicon.GetWidth(), bigicon.GetHeight()))
        BigIconBox.Add(BigIconSizer,0,wx.ALIGN_CENTER |wx.ALL,10)

        fctztf = wx.Image(loadimage("ztf-t.png"),wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        logoupv = wx.Image(loadimage("logoupv-t.png"),wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        ZTFLogoSizer = wx.StaticBitmap(self.panel,-1,fctztf,wx.DefaultPosition,(fctztf.GetWidth(),fctztf.GetHeight()))
        UPVLogoSizer = wx.StaticBitmap(self.panel,-1,logoupv,wx.DefaultPosition,(logoupv.GetWidth(),logoupv.GetHeight()))
        ZTFBox = wx.BoxSizer(wx.HORIZONTAL)
        UPVBox = wx.BoxSizer(wx.HORIZONTAL)

        ZTFBox.Add(ZTFLogoSizer,0,wx.ALIGN_CENTER | wx.ALL,border = 30)
        UPVBox.Add(UPVLogoSizer,0,wx.ALIGN_CENTER | wx.ALL,border = 30)
        
        
        LogoBox = wx.BoxSizer(wx.HORIZONTAL)
        LogoBox.Add(ZTFBox)
        LogoBox.Add(UPVBox)
        

        
        self.panel.Box.Add(BigIconBox,0,wx.CENTER)
        self.panel.Box.Add(TextBox,0,wx.CENTER)
        self.panel.Box.Add(LogoBox,0,wx.CENTER)

        self.panel.SetSizer(self.panel.Box)
        self.panel.SetAutoLayout(True)
        self.panel.Box.Fit(self.panel)


#-----------------------------------------------------------
# SplashScren class copied from wxPython demo (Main.py file)


class MySplashScreen(wx.SplashScreen):
    def __init__(self):
        bmp = wx.Image(loadimage("bigicon.png")).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 2500, None, -1)
        wx.Yield()
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.fc = wx.FutureCall(2000, self.ShowMain)


    def OnClose(self, evt):
        # Make sure the default handler runs too so this window gets
        # destroyed
        evt.Skip()
        self.Hide()
        
        # if the timer is still running then go ahead and show the
        # main frame now
        if self.fc.IsRunning():
            self.fc.Stop()
            self.ShowMain()


    def ShowMain(self):
        mainlogger.info("PROGRAM START")
        frame = MyFrame(None, ID_TOPFRAME, "DiXP: Diffraction with X-Ray on Powder")
        frame.Show()
        if self.fc.IsRunning():
            self.Raise()

        


#------------------------------------------------------------

class MyApp(wx.App):
    def OnInit(self):
        splash = MySplashScreen()
        splash.Show()
        return True
 

#------------------------------------------------------------

def main():
    app = MyApp(False)
    app.MainLoop()



if __name__ == '__main__':
    __name__ = 'control'
    main()

