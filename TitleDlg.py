from control_wdr import *
import wx

from validators import SetValidatorsToWidgets


class TitleDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        TitleDlgFunc( self, True )
         
        self.CurrentConfig = parent.CurrentConfig


        self.values = {}
        self.values["TitleTitle"] = ["title","title",'TextCtrl','Generic']
        self.values["TitleComment"] = ["title","comment",'TextCtrl','Generic']
 
        # Put default values in the dialog.
        for WidgetName in self.values.keys():
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)

            
        #----------------------------------------
        # EVENT TABLE
        #----------------------------------------
        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)



    #----------------------------------------    
    # Getters 
    #----------------------------------------    

    def GetTitleTitleTextCtrl(self):
        return self.FindWindowById(ID_TITLE_TITLE_TEXTCTRL)

    def GetTitleCommentTextCtrl(self):
        return self.FindWindowById(ID_TITLE_COMMENT_TEXTCTRL)

    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnOK(self,event):
        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                value = str(eval('self.GetParent().'+item[0]+'.data'))
                self.CurrentConfig[section][option] = value
#            self.CurrentConfig.write()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return
