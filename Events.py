import wx




EVT_FINISHMEASUREMENT_ID = wx.NewEventType()
EVT_FINISHMEASUREMENT = wx.PyEventBinder(EVT_FINISHMEASUREMENT_ID,1)

class FinishMeasurementEvent(wx.PyEvent):
    def __init__(self):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_FINISHMEASUREMENT_ID)

EVT_GETPOINT_ID = wx.NewEventType()
EVT_GETPOINT = wx.PyEventBinder(EVT_GETPOINT_ID,1)

class GetPointEvent(wx.PyEvent):
    def __init__(self, Data):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_GETPOINT_ID)
        self.Data = Data

EVT_RAISEEXCEPTION_ID = wx.NewEventType()
EVT_RAISEEXCEPTION = wx.PyEventBinder(EVT_RAISEEXCEPTION_ID, 1)

class RaiseExceptionEvent(wx.PyEvent):
    def __init__(self,exception):
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RAISEEXCEPTION_ID)
	self.exception = exception
