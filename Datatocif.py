from CifFile import CifFile, CifBlock

def DataToCif(datafilename,MeasureConfig):
   
    datafile = open(datafilename,"r")
    dataciffilename = datafilename.split(".")[0]+'.cif'
    dataciffile = open(dataciffilename,"w")


    # Get the data
    datablock = []
    datablock.append(["_pd_data_point_id","_pd_meas_2theta_scan","_pd_meas_counts"])

    position = []
    intensity = []
    datapoint = []
    i = 0
    for line in datafile:
        if not line.startswith("#"):
            i += 1
            linedata = line.strip().split()
            datapoint.append(i)
            position.append(linedata[0])
            intensity.append(linedata[1])
    data = [datapoint,position,intensity]
    datablock.append(data)
    datablock = tuple(datablock)

    # Create the block where the data goes.
    measurementblock = block = CifBlock()


    # pd_spec
    scanmode = MeasureConfig['scanmode']['mode']
    if scanmode == 'Debye-Scherrer':
        mount_mode = 'transmission'
    elif scanmode == 'Reflection':
        mount_mode = 'reflection' 

    measurementblock['_pd_spec_mount_mode'] = mount_mode



    # pd_instr
    #_pd_instr_beam_size_ax
    #_pd_instr_beam_size_eq

    measurementblock['_pd_instr_cons_illum_flag']='no'

##     _pd_instr_dist_src/mono
##     _pd_instr_dist_mono/spec
##     _pd_instr_dist_src/spec
##     _pd_instr_dist_spec/detc


##     _pd_instr_divg_ax_src/mono
##     _pd_instr_divg_ax_mono/spec
##     _pd_instr_divg_ax_src/spec
##     _pd_instr_divg_ax_spec/detc

##     _pd_instr_divg_eq_src/mono
##     _pd_instr_divg_eq_mono/spec
##     _pd_instr_divg_eq_src/spec
##     _pd_instr_divg_eq_spec/detc

    
    measurementblock['_pd_instr_geometry'] = "Parallel-beam non-focusing optics with point detector"
    measurementblock['_pd_instr_location'] ="Faculty of Science and Technology \nUniversity of the Basque Country"

    measurementblock['_pd_instr_monochr_pre_spec']= 'Gooble mirror'
##     _pd_instr_monochr_post_spec

##     _pd_instr_slit_ax_src/mono
##     _pd_instr_slit_ax_mono/spec
##     _pd_instr_slit_ax_src/spec
##     _pd_instr_slit_ax_spec/anal
##     _pd_instr_slit_ax_anal/detc
##     _pd_instr_slit_ax_spec/detc

##     _pd_instr_slit_eq_src/mono
##     _pd_instr_slit_eq_mono/spec
##     _pd_instr_slit_eq_src/spec
##     _pd_instr_slit_eq_spec/anal
##     _pd_instr_slit_eq_anal/detc
##     _pd_instr_slit_eq_spec/detc

##     _pd_instr_soller_ax_src/mono
##     _pd_instr_soller_ax_mono/spec
##     _pd_instr_soller_ax_src/spec
##     _pd_instr_soller_ax_spec/anal
##     _pd_instr_soller_ax_anal/detc
##     _pd_instr_soller_ax_spec/detc

##     _pd_instr_soller_eq_src/mono
##     _pd_instr_soller_eq_mono/spec
##     _pd_instr_soller_eq_src/spec
##     _pd_instr_soller_eq_spec/anal
##     _pd_instr_soller_eq_anal/detc
##     _pd_instr_soller_eq_spec/detc

##     _pd_instr_source_size_ax
##     _pd_instr_source_size_eq

##     _pd_instr_special_details

##     _pd_instr_2theta_monochr_pre
##     _pd_instr_2theta_monochr_post

##     pd_meas

    dateandtime = MeasureConfig['times']['startedat']
    date = dateandtime.split(" ")[0]
    time = dateandtime.split(" ")[1]
    measurementblock['_pd_meas_datetime_initiated'] = date+'T'+time+'+A'

    measurementblock['_pd_meas_scan_method'] = 'step'
    measurementblock['_pd_meas_step_count_time'] = MeasureConfig['range']['timestep']


#   DIFRAN_DETECTOR

    measurementblock['_diffrn_detector'] = 'scintillation counter'
    measurementblock['_diffrn_detector_type'] = 'Seifter & Co.'

    #DIFFRN_RADIATION

    measurementblock['_diffrn_radiation_monochromatior'] = 'Goobel mirror'
    measurementblock['_diffrn_radiation_probe'] = 'x-ray'
    measurementblock['_diffrn_radiation_type'] = 'Cu K\\a'
    measurementblock['_diffrn_radiation_xray_symbol'] = 'KL~2,3~'

    #DIFFRN_RADIATION_WEAVELENGTH

    measurementblock['_diffrn_radiation_weavelength'] = '1.5418'

    #DIFFRN_SOURCE
    measurementblock['_diffrn_source'] = 'sealed x-ray tube'
    measurementblock['_diffrn_source_details'] = 'Seifert & Co.'

##  _diffrn_source_take-off_angle
    
    current = MeasureConfig['generator']['current'] # in mA
    voltage = MeasureConfig['generator']['voltage'] # in KV
    power = (int(current)*int(voltage))/1000.
    measurementblock['_diffrn_source_current'] = current
    measurementblock['_diffrn_source_voltage'] = voltage
    measurementblock['_diffrn_source_power'] = str(power)
    measurementblock['_diffrn_source_target'] = 'Cu'
    
    
    # add data
    measurementblock.AddCifItem(datablock)

    # Create the Cif
    cif = CifFile()
    measurementblockname = MeasureConfig['files']['datafile'].split(".")[0]
    cif[measurementblockname] = measurementblock


    # Write the file
    comment = r"""
# This file is created automatically
# by the DiXP program.
"""
    dataciffile.write(comment)
    dataciffile.write(str(cif))


