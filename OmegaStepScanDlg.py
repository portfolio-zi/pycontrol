from control_wdr import *

import matplotlib
matplotlib.use('WX')
# some of this code is numarray dependent
matplotlib.rcParams['numerix'] = 'numarray'  
#import matplotlib.cm as cm
from matplotlib.backends.backend_wxagg import Toolbar, FigureCanvasWxAgg
from matplotlib.figure import Figure
import matplotlib.numerix as numerix
from matplotlib.mlab import meshgrid


from comunicacion import *
from scipy import optimize, arange
import ErrorDlg
from utilfunc import *
import Measure
from Events import *
import validators
import wx
import datetime
from configobj import ConfigObj


ID_MEASUREMENT_TIMER = wx.NewId()

 
class OmegaStepScanDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):

        wx.Dialog.__init__(self, parent, id, title, pos, size, style)




        self.Figure = Figure((5,4), 75)
        self.MyCanvas = FigureCanvasWxAgg(self,ID_OMEGASTEPSCAN_PLOT , self.Figure)
        self.MyCanvasAxes = self.Figure.gca()
        self.MyCanvasAxes.set_xlabel(r'$\omega$')
        self.MyCanvasAxes.set_ylabel("Intensity")
        self.MyCanvasAxes.grid()
        self.MyCanvasAxes.set_title("Alignment")
        
        OmegaStepScanDlgFunc(self, True)


        # Set validators
        self.StepNumber = validators.DataHolder()
        self.GetStepScanStepNumberTextCtrl().SetValidator(validators.IntegerTextValidator(self.StepNumber))
        self.StepSize = validators.DataHolder()
        self.GetStepScanStepSizeTextCtrl().SetValidator(validators.FloatTextValidator(self.StepSize))
        self.Time = validators.DataHolder()
        self.GetStepScanTimeTextCtrl().SetValidator(validators.FloatTextValidator(self.Time))

        # The configuration information
        self.MeasurementConfig = ConfigObj()
        self.MeasurementConfig.merge(parent.DefaultConfig)



        backcolor = self.GetBackgroundColour()
        r,g,b = backcolor.Get()
        backcolor = (r/255.,g/255.,b/255.)
        self.Figure.set_facecolor(backcolor)

      
        self.CentreOnParent()
        
        # The timer will keep how long the measuremnt is.
        self.timer = wx.Timer(self,ID_MEASUREMENT_TIMER)
        self.starttime = None


       # WDR: handler declarations for AlineamientoDlg
        self.Bind(wx.EVT_BUTTON,self.OnStepScanDlg,id=ID_OMEGASTEPSCAN_START_BUTTON)
        self.Bind(wx.EVT_BUTTON,self.OnStop,id=ID_OMEGASTEPSCAN_STOP_BUTTON)
        self.Bind(EVT_GETPOINT,self.GetPointEvent)
        self.Bind(EVT_FINISHMEASUREMENT,self.FinishMeasurement)
        self.Bind(wx.EVT_TIMER,self.RefreshTime,self.timer)


    # WDR: methods for:
    def GetStepScanCenterTextCtrl(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_CENTER_TEXTCTRL)
    
    def GetStepScanThetaTextCtrl(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_TEXTCTRL)

    def GetStepScanStepNumberTextCtrl(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_STEPNUMBER_TEXTCTRL)

    def GetStepScanTimeTextCtrl(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_TIMEPERSTEP_TEXTCTRL)

    def GetStepScanStepSizeTextCtrl(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_STEPSIZE_TEXTCTRL)

    def GetStepScanCalculatedCenterTextCtrl(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_CALCULATEDCENTER_TEXTCTRL)

    def GetStepSizeGauge(self):
        return self.FindWindowById(ID_OMEGASTEPSIZE_GAUGE)

    def GetStepScanStartButton(self):
        return self.FindWindowById(ID_OMEGASTEPSCAN_START_BUTTON)

    def DisableControls(self):
        self.GetStepScanStartButton().Enable(False)
        self.GetStepScanStepSizeTextCtrl().Enable(False)
        self.GetStepScanTimeTextCtrl().Enable(False)
        self.GetStepScanStepNumberTextCtrl().Enable(False)
        self.GetStepScanCalculatedCenterTextCtrl().Enable(False)
        self.GetStepScanCenterTextCtrl().Enable(False)

    def EnableControls(self):
        self.GetStepScanStartButton().Enable(True)
        self.GetStepScanStepSizeTextCtrl().Enable(True)
        self.GetStepScanTimeTextCtrl().Enable(True)
        self.GetStepScanStepNumberTextCtrl().Enable(True)
        self.GetStepScanCalculatedCenterTextCtrl().Enable(True)
        self.GetStepScanCenterTextCtrl().Enable(True)


    def OnStepScan(self):
        self.DisableControls()


        # Get values from dialog with validators.
        if self.Validate() and self.TransferDataFromWindow():
            pass
        else:
            print "Error Validate"
            self.EnableControls()
            return

        # Data for the MeasurementBasic class
        self.IPAddress = self.MeasurementConfig["comunication"]["ip"]
        self.IPPort = int(self.MeasurementConfig["comunication"]["port"])
        Diffractometer = diffracsocket(self.IPAddress,self.IPPort)


        self.CurrentMeasurement = Measure.GetMeasurementClass(self,self.MeasurementConfig)    
        self.CurrentMeasurement.Diffractometer = Diffractometer

        
        self.StepSize = self.StepSize.data
        self.Time = self.Time.data
        self.DetectionTypeNumber = self.Time

        self.StepNumber = self.StepNumber.data
        # Get values from dialog without validators.
        self.Center = self.GetStepScanCenterTextCtrl().GetValue()
        self.TThetaCenter = eval(self.Center.split(",")[0])
        self.ThetaCenter = eval(self.Center.split(",")[1])



        self.ThetaBegin = self.ThetaCenter-self.StepSize*int(self.StepNumber/2.0)
        self.ThetaEnd = self.ThetaCenter+self.StepSize*int(self.StepNumber/2.0)
        self.ThetaStep = self.StepSize


        self.MeasurementConfig["range"]["thetabegin"] = str(self.ThetaBegin)
        self.MeasurementConfig["range"]["thetaend"] = str(self.ThetaEnd)
        self.MeasurementConfig["range"]["thetastep"] = str(self.ThetaStep)
        self.MeasurementConfig["range"]["timestep"] = str(self.Time)
        self.MeasurementConfig["scanmode"]["detectiontype"] = "Time"
        self.MeasurementConfig["scanmode"]["mode"] = "Reflection"
        self.MeasurementConfig["scanmode"]["type"] = "Theta"



        self.Position = []
        self.Intensity = []
        self.CurrentMeasurement.start()



    def GetPointEvent(self, event):
        self.Position.append(event.Data[0])
        self.Intensity.append(event.Data[1])
        self.MyCanvasAxes.plot(self.Position,self.Intensity)
        self.Refresh()



    def RaiseException(self, event):
        print "Enter RaiseException"
        exception = event.exception
        try: 
            raise exception[0]#, exception[1]
        except ConectionError, error:
            print "Enter exception conectione"
            ErrorDlg.ErrorMessage(self, error.Message)
        except InstruccionError, error:
            print "Enter exception instrument"
            ErrorDlg.ErrorMessage(self, error.Message)
        


    def OnStepScanDlg(self, event):
            self.OnStepScan()
            self.starttime = datetime.datetime.now()
            self.now = datetime.datetime.now()
            self.FindWindowById(ID_OMEGASTEPSCAN_STARTAT_TEXT).SetLabel(
                str(self.starttime.ctime()))
##            print "stop"

    def FinishMeasurement(self,event):
        self.timer.Stop()
        parameters = self.FindMaxima(self.Position,self.Intensity)
        maxima = parameters[1]
        maximastr = "%.7f" %(maxima)
        self.EnableControls()
        self.GetStepScanCalculatedCenterTextCtrl().Enable(True)
        self.GetStepScanCalculatedCenterTextCtrl().SetValue(maximastr)
        self.DrawPseudoVoight(parameters)
        self.DrawMaximaLine(parameters)
        print maximastr

    def DrawMaximaLine(self,parameters):
        A,x0,sigma,eta = parameters
        maximatheta = x0
        maximaintensity = pseudovoight(x0,A,x0,sigma,eta)
        self.MyCanvasAxes.axvline(maximatheta,linewidth=2,color="r")


    def DrawPseudoVoight(self,parameters):
        A,x0,sigma,eta = parameters

        thetamin = self.CurrentMeasurement.TThetaBegin
        thetamax =self.CurrentMeasurement.TThetaEnd
        pointsnumber = 1000
        step = (thetamax-thetamin)/pointsnumber
        thetarange = arange(pointsnumber)
        thetarange = thetamin+(thetarange*step)
        intensityrange = pseudovoight(thetarange,A,x0,sigma,eta)
        self.MyCanvasAxes.plot(thetarange,intensityrange,'r')

    def FindMaxima(self, Position, Intensity):
        from pylab import array
        thetarange = array(Position)
        irange = array(Intensity)
        x0 = (self.CurrentMeasurement.TThetaBegin+
              self.CurrentMeasurement.TThetaEnd)/2.0
        param0 = [1000,x0,1,0.7]
        plsq = optimize.leastsq(self.Residuals,param0,args=(irange,thetarange))
        param = plsq[0]
        
        return param
                               
        
    def Residuals(self,parameters,  intensity,theta):
        A,x0,sigma,eta = parameters
        err = intensity - pseudovoight(theta,A,x0,sigma,eta)
        return err

    def OnStop(self, event):
        if self.CurrentMeasurement:
            self.CurrentMeasurement.abort()


    def RefreshTime(self, event):
        self.now = datetime.datetime.now()
        elapse = self.now - self.starttime
        deltastr = str(elapse)
        # chop out the miliseconds.
        #reverse string
        rev = deltastr[::-1]
        index = rev.find(".")+1
        newstring = rev[index:]
        deltastr = newstring[::-1]
        self.FindWindowById(ID_OMEGASTEPSCAN_DURATION_TEXT).SetLabel(deltastr)






    
