from control_wdr import *
import wx

from validators import SetValidatorsToWidgets


class GeneratorDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        GeneratorDlgFunc( self, True )
        
        self.CurrentConfig = parent.CurrentConfig


        self.values = {}

        self.values['GeneratorCurrent'] = ['generator','current','Slider','Generic']
        self.values['GeneratorVoltage'] = ['generator','voltage','Slider','Generic']
        self.values['GeneratorShutdown'] = ['generator','shutdown','CheckBox','Boolean']
        self.values['GeneratorShutter'] = ['generator','opencloseshutter','CheckBox','Boolean']
        self.values['GeneratorNotXray'] = ['generator','notxray','CheckBox','Boolean']

        # Put default values in the dialog.
        for WidgetName in self.values.keys():
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)


        #----------------------------------------
        # EVENT TABLE
        #----------------------------------------
        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)
        
    #----------------------------------------    
    # Getters 
    #----------------------------------------    

    def GetGeneratorVoltageSlider(self):
        return self.FindWindowById(ID_GENERATOR_VOLTAGE_SLIDER)

    def GetGeneratorCurrentSlider(self):
        return self.FindWindowById(ID_GENERATOR_CURRENT_SLIDER)

    def GetGeneratorShutdownCheckBox(self):
        return self.FindWindowById(ID_GENERATOR_SHUTDOWN_CHECKBOX)

    def GetGeneratorShutterCheckBox(self):
        return self.FindWindowById(ID_GENERATOR_OPENCLOSESHUTTER_CHECKBOX)

    def GetGeneratorNotXrayCheckBox(self):
        return self.FindWindowById(ID_GENERATOR_DEBUGXRAY_CHECKBOX)

    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnOK(self,event):
        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                value = str(eval('self.GetParent().'+item[0]+'.data'))
                self.CurrentConfig[section][option] = str(value)
#            self.CurrentConfig.write()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return
