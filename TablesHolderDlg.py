from control_wdr import *
import wx

from validators import SetValidatorsToWidgets


class TablesHolderDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)
        
        # WDR: dialog function DetectorDlgFunc for DetectorDlg
        TablesHolderDlgFunc( self, True )
        

        # Get default values
        self.CurrentConfig = parent.CurrentConfig

        # Put default values in the dialog.
        self.values = {}
        self.values['TthetaVelocity'] = ['tables','tth-speed','Slider','Generic']
        self.values['ThetaVelocity'] = ['tables','th-speed','Slider','Generic']
        self.values['GoHome'] = ['tables','go-home','CheckBox','Boolean']
        self.values['IsHolder'] = ['sampleholder','isholder','CheckBox','Boolean']
        self.values['HolderVelocity'] = ['sampleholder','holderspeed','TextCtrl','FloatText']

         # Put default values in the dialog.
        for WidgetName in self.values.keys():
            WidgetType = self.values[WidgetName][2]
            ValidatorType = self.values[WidgetName][3]
            SetValidatorsToWidgets(self,
                                   WidgetName,WidgetType,ValidatorType)

        #----------------------------------------
        # EVENT TABLE
        #----------------------------------------
        self.Bind(wx.EVT_BUTTON, self.OnOK, id=wx.ID_OK)

        
    #----------------------------------------    
    # Getters 
    #----------------------------------------    

    def GetTthetaVelocitySlider(self):
        return self.FindWindowById(ID_TABLESHOLDER_TTHETA_VELOCITY_SLIDER)

    def GetThetaVelocitySlider(self):
        return self.FindWindowById(ID_TABLESHOLDER_THETA_VELOCITY_SLIDER)

    def GetGoHomeCheckBox(self):
        return self.FindWindowById(ID_TABLESHOLDER_GOHOME_CHECKBOX)
    
    def GetHolderVelocityTextCtrl(self):
        return self.FindWindowById(ID_TABLESHOLDER_HOLDER_VELOCITY_TEXTCTRL)

    def GetIsHolderCheckBox(self):
        return self.FindWindowById(ID_TABLESHOLDER_ISSAMPLE_CHECKBOX)

    #----------------------------------------
    # METHODS
    #----------------------------------------

    def OnOK(self,event):
        if self.Validate() and self.TransferDataFromWindow():
            for item in self.values.items():
                section = item[1][0]
                option = item[1][1]
                value = str(eval('self.GetParent().'+item[0]+'.data'))
                self.CurrentConfig[section][option] = value
#            self.CurrentConfig.write()
            if self.IsModal():
                self.EndModal(wx.ID_OK)
            else:
                self.SetReturnCode(wx.ID_OK);
                self.Show(False);               
        else:
            return
