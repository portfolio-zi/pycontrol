#!/bin/env python
#----------------------------------------------------------------------------
# Name:         Medida.
# Author:       Zunbeltz Izaola
# Created:      11/05/03
# Copyright:    
#----------------------------------------------------------------------------
from control_wdr import *

import matplotlib
matplotlib.use('WX')
matplotlib.rcParams['numerix'] = 'numarray'  
import matplotlib.cm as cm
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
from matplotlib.figure import Figure
#import matplotlib.numerix as numerix
#from matplotlib.mlab import meshgrid
from GraphicDlg import MyNavigationToolbar



from comunicacion import diffracsocket
from Datatocif import DataToCif
import ErrorDlg
from utilfunc import *
import Measure
from Events import *
from Globals import *
from configobj import ConfigObj

import validators
import wx
import datetime
import traceback
import os.path
import os


ID_MEASUREMENT_TIMER = wx.NewId()


class MeasureDlg(wx.Dialog):
    def __init__(self, parent, id, title,
        pos = wx.DefaultPosition, size = wx.DefaultSize,
        style = wx.DEFAULT_DIALOG_STYLE ):
        wx.Dialog.__init__(self, parent, id, title, pos, size, style)

#        self.dfconf = parent.dfconf

        # Create the canvas where the plot will go
        self.Figure = Figure((5,4), 75)
        self.MyCanvas = FigureCanvasWxAgg(self,ID_MEASUREMENT_PLOT , self.Figure)
        DefaultConfig = parent.DefaultConfig
        self.CurrentConfig = parent.CurrentConfig


        self.MeasurementConfig = ConfigObj()
        self.MeasurementConfig.merge(DefaultConfig)
        self.MeasurementConfig.merge(self.CurrentConfig)

        self.MyCanvasAxes = self.Figure.gca()
        self.MyCanvasAxes.set_ylabel("Intensity")
        self.MyCanvasAxes.grid()
        self.MyCanvasAxes.set_title("Measurement")

        # Put correct x axis label depending of scan type
        scantype = self.MeasurementConfig["scanmode"]["type"]
        if scantype in ["Theta: 2 Theta","2 Theta","Theta : Theta"]:
            self.MyCanvasAxes.set_xlabel(r'$2 \theta$')
            rangemin = float(self.MeasurementConfig["range"]["2thetabegin"])
            rangemax = float(self.MeasurementConfig["range"]["2thetaend"])
            self.MyCanvasAxes.set_xlim(rangemin,rangemax)
        elif scantype in ["Theta"]:
            self.MyCanvasAxes.set_xlabel(r'$\theta$')
            rangemin = float(self.MeasurementConfig["range"]["thetabegin"])
            rangemax = float(self.MeasurementConfig["range"]["thetaend"])
            self.MyCanvasAxes.set_xlim(rangemin,rangemax)


        # set figure background colour like the colour of the dialog.
        backcolor = self.GetBackgroundColour()
        r,g,b = backcolor.Get()
        backcolor = (r/255.,g/255.,b/255.)
        self.Figure.set_facecolor(backcolor)

        # The timer will keep how long the measuremnt is.
        self.timer = wx.Timer(self,ID_MEASUREMENT_TIMER)
        self.starttime = None


        # Build the dialog
        MeasurementDlgFunc(self, True)
        self.add_toolbar(self.MyCanvas)
        self.Fit()
        self.CentreOnParent()
        
        # WDR: handler declarations for MeasureDlg
        self.Bind(wx.EVT_BUTTON,self.OnStop,id=ID_MEASUREMENT_STOP_BUTTON)
        self.Bind(wx.EVT_BUTTON,self.OnMeasurementDlg,id=ID_MEASUREMENT_MEASURE_BUTTON)
        self.Bind(EVT_GETPOINT,self.GetPointEvent)
        self.Bind(EVT_RAISEEXCEPTION,self.RaiseException)
        self.Bind(EVT_FINISHMEASUREMENT,self.FinishMeasurement)
        self.Bind(wx.EVT_TIMER,self.RefreshTime,self.timer)

##         self.CURRENTUSER = USERCONF["user-info"]["currentusername"]
##         self.CURRENTDIR = USERCONF["user-info"]["currentdir"]



    def OnMeasurementDlg(self, event):
        # Start Measurement
        self.OnMeasurement()

        # Put starting time on the dialog
        self.starttime = datetime.datetime.now()
        self.CurrentConfig["times"]["startedat"] = str(self.starttime)
        self.now = self.starttime
        self.FindWindowById(ID_MEASUREMENT_STARTAT_TEXT).SetLabel(
            str(self.starttime.ctime()))




    def OnMeasurement(self):
        # data file
        self.FileName = self.MeasurementConfig["files"]["datafile"]
        absolutepath = os.path.abspath(self.CurrentConfig.filename)
        basedir = os.path.dirname(absolutepath)
        self.FileName = os.path.join(basedir,self.FileName)

        # Open Data file
        self.File = open(self.FileName,"w")
        header = self.GetFileHeader()
        self.File.write(header)
        self.File.close()
        self.Position = []
        self.Intensity = []
            
        # Set Measurement
        self.IPAddress = self.MeasurementConfig["comunication"]["ip"]
        self.IPPort = int(self.MeasurementConfig["comunication"]["port"])
        Diffractometer = diffracsocket(self.IPAddress,self.IPPort)
        self.CurrentMeasurement = Measure.GetMeasurementClass(self,self.MeasurementConfig)    
        self.CurrentMeasurement.Diffractometer = Diffractometer
        self.CurrentMeasurement.start()


    def OnStop(self, event):
        if self.CurrentMeasurement:
            self.CurrentMeasurement.abort()


    def RefreshTime(self, event):
        self.now = datetime.datetime.now()
        elapse = self.now - self.starttime
        deltastr = str(elapse)
        # chop out the miliseconds.
        #reverse string
        rev = deltastr[::-1]
        index = rev.find(".")+1
        newstring = rev[index:]
        deltastr = newstring[::-1]
        self.FindWindowById(ID_MEASUREMENT_DURATION_TEXT).SetLabel(deltastr)
        

    def GetFileHeader(self):
        buffer = ""
        delimeter = "#"*70+"\n"
        buffer += delimeter
        text = self.GetParent().textbox.GetValue()
        lines = text.split("\n")
        for l in lines:
            line = "# "+l+"\n"
            buffer += line
        buffer += delimeter
        return buffer


    def FinishMeasurement(self, event):
        self.timer.Stop()
        self.endtime = datetime.datetime.now()
        self.CurrentConfig["times"]["finishedat"] = str(self.endtime)
        self.duration = self.endtime-self.starttime
        self.CurrentConfig["times"]["duration"] = str(self.duration)
        self.CurrentConfig.write()
        self.GetParent().PrintResume(self.CurrentConfig)
        self.RewriteDataFile()
        self.OutputCif()

    def OutputCif(self):
        DataToCif(self.FileName,self.MeasurementConfig)

    def RewriteDataFile(self):
        backfilename = self.FileName +'.back'
        File = open(self.FileName,"r")
        BackFile = open(backfilename,"w")
        BackFile.write(self.GetFileHeader())
        for  line in File:
            if line.startswith("#"):
                pass
            else:
                BackFile.write(line)
        File.close()
        BackFile.close()
        os.rename(backfilename,self.FileName)


        
    def GetPointEvent(self, event):
        self.File = open(self.FileName,"a")
        self.Position.append(event.Data[0])
        self.Intensity.append(event.Data[1])
        self.MyCanvasAxes.plot(self.Position,self.Intensity)
        self.Refresh()

        buffer = str(event.Data[0])+" "+str(event.Data[1])+"\n"
        self.File.write(buffer)
        self.File.close()
        

    def RaiseException(self, event):
        exception = event.exception
        try: 
            raise exception
        except:
            # Exception formating taken from Python Cookbook 14.3
            import traceback
            type, value, tb = exception
            limit = None
            infolist = traceback.format_tb(tb, limit) + traceback.format_exception_only(type,value)
            body = "Traceback (innermost last):\n" + "%-20s %s" % (
                "".join(infolist[:-1]),infolist[-1])

            ErrorDlg.ErrorMessage(self,body)


    def add_toolbar(self, canvas):
        self.toolbar = MyNavigationToolbar(canvas,True)
        self.toolbar.Realize()
        if wx.Platform == '__WXMAC__':
            # Mac platform (OSX 10.3, MacPython) does not seem to cope with
            # having a toolbar in a sizer. This work-around gets the buttons
            # back, but at the expense of having the toolbar at the top
            self.SetToolBar(self.toolbar)
        else:
            # On Windows platform, default window size is incorrect, so set
            # toolbar width to figure width.
            tw, th = self.toolbar.GetSizeTuple()
            fw, fh = canvas.GetSizeTuple()
            # By adding toolbar in sizer, we are able to put it at the bottom
            # of the frame - so appearance is closer to GTK version.
            # As noted above, doesn't work for Mac.
            self.toolbar.SetSize(wx.Size(fw, th))
            self.MeasurementSizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        # update the axes menu on the toolbar
        self.toolbar.update()  


